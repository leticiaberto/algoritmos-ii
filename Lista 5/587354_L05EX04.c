/* ================================================================== *
   Universidade Federal de Sao Carlos - UFSCar, Sorocaba

   Disciplina: Algoritmos e Programação 2
   Prof. Tiago A. Almeida

   Lista 05 - Exercício 04 - Cadastro de Super-Herois

   Instrucoes
   ----------

   Este arquivo contem o codigo que auxiliara no desenvolvimento do
   exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
   Dados do aluno:

   RA: 587354
   Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// SAIR ****************** //
#define SAIR               0
// HEROI ***************** //    
#define ADICIONAR_HEROI    11
#define ALTERAR_HEROI      12
#define EXCLUIR_HEROI      13
#define CONSULTAR_HEROI    14
// PODER ***************** //
#define ADICIONAR_PODER    21
#define ALTERAR_PODER      22
#define EXCLUIR_PODER      23
#define CONSULTAR_PODER    24
#define ASSOCIAR_PODER     25
// EVENTO **************** //
#define ADICIONAR_EVENTO   31
#define ALTERAR_EVENTO     32
#define EXCLUIR_EVENTO     33
#define CONSULTAR_EVENTO   34
#define ASSOCIAR_EVENTO    35

// MENSAGENS ******************************************************** //
#define HEROI_CADASTRADO "O heroi foi cadastrado com sucesso.\n"
#define HEROI_ALTERADO "O heroi foi alterado com sucesso.\n"
#define HEROI_EXCLUIDO "O heroi foi excluido com sucesso.\n"
#define COD_HEROI_JA_CADASTRADO "Ja existe um heroi com este codigo.\n"
#define COD_HEROI_NAO_CADASTRADO "Nao existe um heroi com este codigo.\n"
#define PODER_CADASTRADO "O poder foi cadastrado com sucesso.\n"
#define PODER_ALTERADO "O poder foi alterado com sucesso.\n"
#define PODER_EXCLUIDO "O poder foi excluido com sucesso.\n"
#define PODER_ASSOCIADO "O poder e o heroi foram associados.\n"
#define COD_PODER_JA_CADASTRADO "Ja existe um poder com este codigo.\n"
#define COD_PODER_NAO_CADASTRADO "Nao existe um poder com este codigo.\n"
#define PROBLEMA_ASSOCIAR_PODER "Houve um problema ao tentar associar o poder.\n"
#define EVENTO_CADASTRADO "O evento foi cadastrado com sucesso.\n"
#define EVENTO_ALTERADO "O evento foi alterado com sucesso.\n"
#define EVENTO_EXCLUIDO "O evento foi excluido com sucesso.\n"
#define EVENTO_ASSOCIADO "O evento e o heroi foram associados.\n"
#define COD_EVENTO_JA_CADASTRADO "Ja existe um evento com este codigo.\n"
#define COD_EVENTO_NAO_CADASTRADO "Nao existe um evento com este codigo.\n"
#define PROBLEMA_ASSOCIAR_EVENTO "Houve um problema ao tentar associar o evento.\n"

// VARIAVEIS DE TAMANHO //
#define MAX_NOME 100
#define MAX_HEROI 20
#define MAX_EVENTO 20
#define MAX_PODER 60

typedef struct poder{
   int codPoder;
   char nome[MAX_NOME];
   int tempoRecarga; // medido em minutos
} Poder;

typedef struct heroi{
   int codHeroi;
   char nome[MAX_NOME];
   int ano;
   Poder *poder;
} Heroi;

typedef struct evento{
   int codEvento;
   char nome[MAX_NOME];
   int ano;
   int numHerois;
   Heroi *heroisEnvolvidos[MAX_HEROI];
} Evento;


// declaracoes das funcoes
/* <<< DECLARE OS PROTOTIPOS DAS FUNCOES AQUI >>> */
int adicionaHeroi(Heroi herois[], int *numHerois, int codHeroi, int ano, char nome[]);
void alteraHeroi(Heroi herois[], int numHerois, int codHeroi, int ano, char nome[]);
int excluiHeroi(Heroi herois[], int *numHerois, int codHeroi);
int consultaHeroi(Heroi herois[], int numHerois, int codHeroi);
int adicionaPoder(Poder poderes[], int *numPoderes, int codPoder, int tempoRecarga, char nome[]);
void alteraPoder(Poder poderes[], int numPoderes, int codPoder, int tempoRecarga, char nome[]);
int excluiPoder(Poder poderes[], int *numPoderes, int codPoder);
int consultaPoder(Poder poderes[], int numPoderes, int codPoder);
void associaPoder(Heroi herois[], int numHerois, Poder poderes[], int numPoderes, int codHeroi, int codPoder);
int adicionaEvento(Evento eventos[], int *numEventos, int codEvento, int ano, char nome[]);
void alteraEvento(Evento eventos[], int numEventos, int codEvento, int ano, char nome[]);
int excluiEvento(Evento eventos[], int *numEventos, int codEvento);
int consultaEvento(Evento eventos[], int numEventos, int codEvento);
void imprimeHeroisPorEvento(Heroi *heroisEnvolvidos[], int numHerois);
void associaEvento(Evento eventos[], int numEventos, Heroi herois[], int numHerois, int codEvento, int codHeroi);
/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main(){

   Heroi herois[MAX_HEROI]; // vetor com todos herois do sistema
   Poder poderes[MAX_PODER]; // vetor com todos herois do sistema
   Evento eventos[MAX_EVENTO]; // vetor com todos herois do sistema

   int numHerois = 0, numPoderes = 0, numEventos = 0;
   int indice;

   // Variaveis para receber os valores de entrada
   int codHeroi, codEvento, codPoder, codPoder2, codPoder3, tempoRecarga, ano;
   char nome[MAX_NOME];

   int opcao = 1;
   while(opcao != 0){
      scanf("%d",&opcao);
      switch(opcao){
         case ADICIONAR_HEROI:
            scanf("%d %d %[^\n]s",&codHeroi, &ano, nome);
            adicionaHeroi(herois, &numHerois, codHeroi, ano, nome);
         break;

         case ALTERAR_HEROI:
            scanf("%d %d %[^\n]s",&codHeroi, &ano, nome);
            alteraHeroi(herois, numHerois, codHeroi, ano, nome);
         break;

         case EXCLUIR_HEROI:
            scanf("%d",&codHeroi);
            excluiHeroi(herois, &numHerois, codHeroi);
         break;

         case CONSULTAR_HEROI:
            scanf("%d",&codHeroi);
            indice = consultaHeroi(herois, numHerois, codHeroi);
            if (indice >= 0) {
               printf("Cod: %d - Ano: %d - Heroi: %s\n", herois[indice].codHeroi, herois[indice].ano, herois[indice].nome);  
            } else {
               printf(COD_HEROI_NAO_CADASTRADO);
            }
         break;

         case ADICIONAR_PODER:
            scanf("%d %d %[^\n]s",&codPoder, &tempoRecarga, nome);
            adicionaPoder(poderes, &numPoderes, codPoder, tempoRecarga, nome);
         break;

         case ALTERAR_PODER:
            scanf("%d %d %[^\n]s",&codPoder, &tempoRecarga, nome);
            alteraPoder(poderes, numPoderes, codPoder, tempoRecarga, nome);
         break;

         case EXCLUIR_PODER:
            scanf("%d",&codPoder);
            excluiPoder(poderes, &numPoderes, codPoder);
         break;

         case CONSULTAR_PODER:
            scanf("%d",&codPoder);
            indice = consultaPoder(poderes, numPoderes, codPoder);
            if (indice >= 0) {
               printf("Cod: %d - Tempo Recarga: %d - Poder: %s\n",
                          poderes[indice].codPoder, poderes[indice].tempoRecarga, poderes[indice].nome);  
            } else {
               printf(COD_PODER_NAO_CADASTRADO);
            }
         break;

         case ASSOCIAR_PODER:
            scanf("%d %d",&codHeroi, &codPoder);
            associaPoder(herois, numHerois, poderes, numPoderes, codHeroi, codPoder);
         break;

         case ADICIONAR_EVENTO:
            scanf("%d %d %[^\n]s",&codEvento, &ano, nome);
            adicionaEvento(eventos, &numEventos, codEvento, ano, nome);
         break;

         case ALTERAR_EVENTO:
            scanf("%d %d %[^\n]s",&codEvento, &ano, nome);
            alteraEvento(eventos, numEventos, codEvento, ano, nome);
         break;

         case EXCLUIR_EVENTO:
            scanf("%d",&codEvento);
            excluiEvento(eventos, &numEventos, codEvento);
         break;

         case CONSULTAR_EVENTO:
            scanf("%d",&codEvento);
            indice = consultaEvento(eventos, numEventos, codEvento);
            if (indice >= 0) {
               printf("Cod: %d - Ano: %d - Evento: %s\n", 
                        eventos[indice].codEvento, eventos[indice].ano, eventos[indice].nome);
               imprimeHeroisPorEvento(eventos[indice].heroisEnvolvidos, eventos[indice].numHerois);
            } else {
               printf(COD_EVENTO_NAO_CADASTRADO);
            }
         break;

         case ASSOCIAR_EVENTO:
            scanf("%d %d",&codEvento, &codHeroi);
            associaEvento(eventos, numEventos, herois, numHerois, codEvento, codHeroi);
         break;
      }
   }
  
   return 0;
}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */
int adicionaHeroi(Heroi herois[], int *numHerois, int codHeroi, int ano, char nome[])
{
	int i;

	for (i=0;i < *numHerois;i++)
	{	
		if (herois[i].codHeroi == codHeroi)
		{	printf(COD_HEROI_JA_CADASTRADO);
			return 0;
		}
	}

	herois[*numHerois].codHeroi = codHeroi;
	herois[*numHerois].ano = ano;
	for (i=0; nome[i] != '\0';i++)
		herois[*numHerois].nome[i] = nome[i];
	
	//printf("Codigo = %d\nAno = %d\nNome = %s\n", herois[*numHerois].codHeroi, herois[*numHerois].ano, herois[*numHerois].nome);
	(*numHerois)++;
	printf(HEROI_CADASTRADO);

}
void alteraHeroi(Heroi herois[], int numHerois, int codHeroi, int ano, char nome[])
{
	int flag=0,i,k;
	for (i=0;i < numHerois;i++)
	{	
		if (herois[i].codHeroi == codHeroi)
		{	herois[i].codHeroi = codHeroi;
			herois[i].ano = ano;			
				
			for (k=0; nome[k] != '\0';k++)
				herois[i].nome[k] = nome[k];
			herois[i].nome[k]='\0';//fechar o vetor de nomes
			
			printf (HEROI_ALTERADO);
			flag=1;
		}
	}
	//printf("Codigo = %d\nAno = %d\nNome = %s\n", herois[numHerois].codHeroi, herois[numHerois].ano, herois[numHerois].nome);
	if (flag == 0)	
		printf(COD_HEROI_NAO_CADASTRADO);
}
int excluiHeroi(Heroi herois[], int *numHerois, int codHeroi)
{
	int i, j,aux=0; 
	for (i = 0; i < *numHerois; i++) 
	{ 
		if (herois[i].codHeroi == codHeroi)
 		{ 
			for (j = i; j < *numHerois; j++) 
				herois[j] = herois[j + 1]; 
			(*numHerois)--;
			printf (HEROI_EXCLUIDO);
			return 0;
		} 
	} 
	
	printf(COD_HEROI_NAO_CADASTRADO);
	return 0;	
}
int consultaHeroi(Heroi herois[], int numHerois, int codHeroi)
{
	int i;
	for (i=0;i < numHerois;i++)
	{	
		if (herois[i].codHeroi == codHeroi)
			return (i);
	}
	
	return(-1);//nao encontrou o código
}
int adicionaPoder(Poder poderes[], int *numPoderes, int codPoder, int tempoRecarga, char nome[])
{
	int i;

	for (i=0;i < *numPoderes;i++)
	{	
		if (poderes[i].codPoder == codPoder)
		{	printf(COD_PODER_JA_CADASTRADO);
			return 0;
		}
	}

	poderes[*numPoderes].codPoder = codPoder;
	poderes[*numPoderes].tempoRecarga = tempoRecarga;
	for (i=0; nome[i] != '\0';i++)
		poderes[*numPoderes].nome[i] = nome[i];
	
	//printf("Codigo = %d\ntempo = %d\nNome = %s\n", poderes[*numPoderes].codPoder, poderes[*numPoderes].tempoRecarga, poderes[*numPoderes].nome);
	(*numPoderes)++;
	printf(PODER_CADASTRADO);
}
void alteraPoder(Poder poderes[], int numPoderes, int codPoder, int tempoRecarga, char nome[])
{
	int flag=0,i,k;
	for (i=0;i < numPoderes;i++)
	{	
		if (poderes[i].codPoder == codPoder)
		{	poderes[i].codPoder = codPoder;
			poderes[i].tempoRecarga = tempoRecarga;			
				
			for (k=0; nome[k] != '\0';k++)
				poderes[i].nome[k] = nome[k];
			poderes[i].nome[k]='\0';//fechar o vetor de nomes
			
			printf (PODER_ALTERADO);
			flag=1;
		}
	}
	//printf("Codigo = %d\ntempo = %d\nNome = %s\n", poderes[numPoderes].codPoder, poderes[numPoderes].tempoRecarga, poderes[numPoderes].nome);
	if (flag == 0)	
		printf(COD_PODER_NAO_CADASTRADO);
}
int excluiPoder(Poder poderes[], int *numPoderes, int codPoder)
{
	int i, j,aux=0; 
	for (i = 0; i < *numPoderes; i++) 
	{ 
		if (poderes[i].codPoder == codPoder)
 		{ 
			for (j = i; j < *numPoderes; j++) 
				poderes[j] = poderes[j + 1]; 
			(*numPoderes)--;
			printf (PODER_EXCLUIDO);
			return 0;
		} 
	} 
	
	printf(COD_PODER_NAO_CADASTRADO);
	return 0;
}
int consultaPoder(Poder poderes[], int numPoderes, int codPoder)
{
	int i;
	for (i=0;i < numPoderes;i++)
	{	
		if (poderes[i].codPoder == codPoder)
			return (i);
	}
	
	return(-1);//nao encontrou o código
}
void associaPoder(Heroi herois[], int numHerois, Poder poderes[], int numPoderes, int codHeroi, int codPoder)
{
	int i,flagpoder=0,flagheroi=0,indpod,indher;

	//verifica se tem o codigo do heroi
	for (i=0;i < numHerois;i++)
	{	
		if (herois[i].codHeroi == codHeroi)
		{
			flagheroi=1;
			indher = i;
		}		
	}

	//verifica se tem o codigo do poder
	for (i=0;i < numPoderes;i++)
	{	
		if (poderes[i].codPoder == codPoder)
		{
			flagpoder=1;	
			indpod = i;
		}	
	}
	
	//associa ou nao o poder e heroi
	if (flagpoder == 1 && flagheroi == 1)
	{
		herois[indher].poder = &poderes[indpod];
		printf(PODER_ASSOCIADO);
	}
	else	
		printf(PROBLEMA_ASSOCIAR_PODER);
}
int adicionaEvento(Evento eventos[], int *numEventos, int codEvento, int ano, char nome[])
{
	int i;
	
	for (i=0;i < *numEventos;i++)
	{	
		if (eventos[i].codEvento == codEvento)
		{	printf(COD_EVENTO_JA_CADASTRADO);
			return 0;
		}
	}

	eventos[*numEventos].codEvento = codEvento;
	eventos[*numEventos].ano = ano;
	eventos[*numEventos].numHerois = 0;
	for (i=0; nome[i] != '\0';i++)
		eventos[*numEventos].nome[i] = nome[i];
	
	//printf("Codigo = %d\ntempo = %d\nNome = %s\n", eventos[*numEventos].codEvento, eventos[*numEventos].ano, eventos[*numEventos].nome);
	(*numEventos)++;
	printf(EVENTO_CADASTRADO);
}
void alteraEvento(Evento eventos[], int numEventos, int codEvento, int ano, char nome[])
{
	int flag=0,i,k;
	for (i=0;i < numEventos;i++)
	{	
		if (eventos[i].codEvento == codEvento)
		{	eventos[i].codEvento = codEvento;
			eventos[i].ano = ano;			
				
			for (k=0; nome[k] != '\0';k++)
				eventos[i].nome[k] = nome[k];
			eventos[i].nome[k]='\0';//fechar o vetor de nomes
			
			printf (EVENTO_ALTERADO);
			flag=1;
		}
	}
	//printf("Codigo = %d\ntempo = %d\nNome = %s\n", eventos[numEventos].codEvento, eventos[numEventos].ano, eventos[numEventos].nome);
	if (flag == 0)	
		printf(COD_EVENTO_NAO_CADASTRADO);
		
		
		  	
}
int excluiEvento(Evento eventos[], int *numEventos, int codEvento)
{
	int i, j,aux=0; 
	for (i = 0; i < *numEventos; i++) 
	{ 
		if (eventos[i].codEvento == codEvento)
 		{ 
			for (j = i; j < *numEventos; j++) 
				eventos[j] = eventos[j + 1]; 
			(*numEventos)--;
			printf (EVENTO_EXCLUIDO);
			return 0;
		} 
	} 
	
	printf(COD_EVENTO_NAO_CADASTRADO);
	return 0;	
}
int consultaEvento(Evento eventos[], int numEventos, int codEvento)
{
	int i;
	for (i=0;i < numEventos;i++)
	{	
		if (eventos[i].codEvento == codEvento)
			return (i);
	}
	
	return(-1);//nao encontrou o código
}
void associaEvento(Evento eventos[], int numEventos, Heroi herois[], int numHerois, int codEvento, int codHeroi)
{
	int i,flagevento=0,flagheroi=0,indheroi = 0, indevento = 0;

	//verifica se tem o codigo do heroi
	for (i=0;i < numHerois;i++)
	{	
		if (herois[i].codHeroi == codHeroi)
		{	
			flagheroi=1;	
			indheroi = i;
		}	
	}

	//verifica se tem o codigo do poder
	for (i=0;i < numEventos;i++)
	{	
		if (eventos[i].codEvento == codEvento)
		{
			flagevento=1;
			indevento = i;
		}		
	}
	
	//associa ou nao o evento e heroi
	if (flagevento == 1 && flagheroi == 1)
	{
		eventos[indevento].heroisEnvolvidos[eventos[indevento].numHerois] = &herois[indheroi];
		//printf("%s",eventos[indevento].heroisEnvolvidos[0]->nome);
		eventos[indevento].numHerois++;
		printf(EVENTO_ASSOCIADO);
	}
	else	
		printf(PROBLEMA_ASSOCIAR_EVENTO);
}
void imprimeHeroisPorEvento(Heroi *heroisEnvolvidos[], int numHerois)
{
	int i;
	for (i=0;i < numHerois;i++)
		printf("%s\n", heroisEnvolvidos[i]->nome);                    
                //printf("%s\n",heroisEnvolvidos[numHerois]->nome);   //da mais ou menos certo    
}
