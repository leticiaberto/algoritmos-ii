/* ================================================================== *
   Universidade Federal de Sao Carlos - UFSCar, Sorocaba

   Disciplina: Algoritmos e Programação 2
   Prof. Tiago A. Almeida

   Lista 05 - Exercício 03 - Super Trunfo

   Instrucoes
   ----------

   Este arquivo contem o codigo que auxiliara no desenvolvimento do
   exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
   Dados do aluno:

   RA: 587354
   Nome: Leticia Mara Berto

* ================================================================== */


#include <stdio.h>
#include <stdlib.h>

#define TOTAL_CARTAS 60

#define ATAQUE     0
#define DEFESA     1
#define VELOCIDADE 2
#define MAGIA      3

// Definicao do registro
/* <<< DECLARE O REGISTRO AQUI >>> */
typedef struct _cartas
{
	int ataque;
	int defesa;
	int velocidade;
	int magia;
}Carta;

// declaracoes das funcoes
void defineVencedor(Carta *jogador1[], Carta *jogador2[], int qtTurnos);

/* <<< DECLARE OS PROTOTIPOS DAS FUNCOES AQUI >>> */
void PerdeuCarta (Carta *jogador[], Carta **carta);
void GanhouCarta (Carta *jogador[], Carta *carta);
int comparaCartas(Carta *jogador1[], Carta *jogador2[], int atributoEscolhido);
/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main() {

   Carta baralho[TOTAL_CARTAS]; // vetor que armazena todas as cartas existentes
   Carta *jogador1[TOTAL_CARTAS]; // vetor que armazena as cartas do jogador 1
   Carta *jogador2[TOTAL_CARTAS]; // vetor que armazena as cartas do jogador 2
   int qtTurnos = 0; // armazena quantidade de turnos do jogo

   int atributoEscolhido, // atributo escolhido pelo juiz a cada turno
       indice,            // indice do baralho que representa qual carta o jogador pega
       i = 0;

   // le a entrada das cartas
   for (i=0; i < TOTAL_CARTAS; i++) {
      scanf("%d",&baralho[i].ataque);
      scanf("%d",&baralho[i].defesa);
      scanf("%d",&baralho[i].velocidade);
      scanf("%d",&baralho[i].magia);
   }

   // divide as cartas entre os dois jogadores
   for (i=0; i < TOTAL_CARTAS/2; i++) {
      scanf("%d",&indice);
      jogador1[i] = &baralho[indice];
      scanf("%d",&indice);
      jogador2[i] = &baralho[indice];
   }

   // aponta as demais posicoes para null
   for (i=TOTAL_CARTAS/2; i < TOTAL_CARTAS; i++) {
      jogador1[i] = NULL;
      jogador2[i] = NULL;
   }

   // recebe os atributos escolhidos pelo juiz, compara quem venceu o turno,
   // o vencedor fica com as cartas e repete ateh um dos jogadores ficar sem cartas.
   while (jogador1[0] != NULL && jogador2[0] != NULL) {
	//printf("%d%d%d%d ", jogador1[0]->ataque, jogador1[0]->defesa, jogador1[0]->velocidade, jogador1[0]->magia);
	//printf("%d%d%d%d\n", jogador2[0]->ataque, jogador2[0]->defesa, jogador2[0]->velocidade, jogador2[0]->magia);
      scanf("%d",&atributoEscolhido);
      comparaCartas(jogador1, jogador2, atributoEscolhido); // realiza um turno: compara as cartas dos dois jogadores
      //com base no atributo escolhido. O vencedor ganha a carta do oponente. Empate: ambos descartam as cartas.
      qtTurnos++;
   }

   defineVencedor(jogador1, jogador2, qtTurnos);

   return 0;

}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */

void defineVencedor(Carta *jogador1[], Carta *jogador2[], int qtTurnos) {

   if (jogador1[0] == NULL && jogador2[0] == NULL) // se nenhum jogador tem carta, o jogo empatou
      printf("Empate!\n");
   else if (jogador1[0] != NULL) // se o jogador1 ainda tem cartas, ele ganhou
      printf("Jogador 1 venceu!\n");
   else
      printf("Jogador 2 venceu!\n");

   printf("Turnos: %d", qtTurnos);

}
int comparaCartas(Carta *jogador1[], Carta *jogador2[], int atributoEscolhido)
{
	Carta *carta;
	int i;
	switch (atributoEscolhido)
	{
		case ATAQUE:
			if (jogador1[0]->ataque == jogador2[0]->ataque)
			{	PerdeuCarta (jogador1,&carta);
				PerdeuCarta (jogador2,&carta);
			}
			else
				if (jogador1[0]->ataque > jogador2[0]->ataque)		
				{	PerdeuCarta (jogador2,&carta);
					GanhouCarta (jogador1,carta);
				}
				else
				{	PerdeuCarta (jogador1,&carta);
					GanhouCarta (jogador2,carta);
					
				}
			break;

		case DEFESA:
			if (jogador1[0]->defesa == jogador2[0]->defesa)
			{	PerdeuCarta (jogador1,&carta);
				PerdeuCarta (jogador2,&carta);
			}
			else
				if (jogador1[0]->defesa > jogador2[0]->defesa)		
				{	PerdeuCarta (jogador2,&carta);
					GanhouCarta (jogador1,carta);
				}
				else
				{	PerdeuCarta (jogador1,&carta);
					GanhouCarta (jogador2,carta);
				}
			break;

		case VELOCIDADE:
			if (jogador1[0]->velocidade == jogador2[0]->velocidade)
			{	PerdeuCarta (jogador1,&carta);
				PerdeuCarta (jogador2,&carta);
			}
			else
				if (jogador1[0]->velocidade > jogador2[0]->velocidade)		
				{	PerdeuCarta (jogador2,&carta);
					GanhouCarta (jogador1,carta);
				}
				else
				{	PerdeuCarta (jogador1,&carta);
					GanhouCarta (jogador2,carta);
				}
			break;

		case MAGIA:
			if (jogador1[0]->magia == jogador2[0]->magia)
			{	PerdeuCarta (jogador1,&carta);
				PerdeuCarta (jogador2,&carta);
			}
			else
				if (jogador1[0]->magia > jogador2[0]->magia)		
				{	PerdeuCarta (jogador2,&carta);
					GanhouCarta (jogador1,carta);
				}
				else
				{	PerdeuCarta (jogador1,&carta);
					GanhouCarta (jogador2,carta);
				}
			break;
	} 
}

void PerdeuCarta (Carta *jogador[], Carta **carta)
{
	int i;
	*carta = jogador[0];

	/*if (jogador[1] == NULL) {
		jogador[0] = NULL;//perder a ultima carta
	}
	else
	{*/
		//perdeu carta
		for (i=1; jogador[i] != NULL; i++)
			jogador[i-1] = jogador[i];//reordena o vetor
		
		jogador[i-1] = NULL;
	//}
		
}

void GanhouCarta (Carta *jogador[], Carta *carta)
{
	int i;//igual a 1 para o caso de um jogador ter apenas uma carta na mao e ganhar outra
	Carta *aux = jogador[0];
/*
	if (jogador[1] == NULL)//igual a 1 para o caso de um jogador ter apenas uma carta na mao e ganhar outra
	{	jogador[0] = aux;
		jogador[1] = carta;
	}
	else
	{*/
	//ganha carta
		for (i=1; jogador[i] != NULL; i++)
		{
			jogador[i-1] = jogador[i];//reordena o vetor
		}//só para percorrer o vetor ate chegar na ultima carta

		jogador[i-1] = aux;//guarda a carta dele
		jogador[i] = carta;//guarda a carta do adversario
	//}
}

