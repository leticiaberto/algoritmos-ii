
/* ================================================================== *
   Universidade Federal de Sao Carlos - UFSCar, Sorocaba

   Disciplina: Algoritmos e Programação 2
   Prof. Tiago A. Almeida

   Lista 05 - Exercício 01 - Calendario

   Instrucoes
   ----------

   Este arquivo contem o codigo que auxiliara no desenvolvimento do
   exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
   Dados do aluno:

   RA: 587354
   Nome: Leticia Mara Berto

* ================================================================== */

#include <stdio.h>

#define PRIMAVERA 0
#define OUTONO    1
#define VERAO     2
#define INVERNO   3

#define TOTAL_DATAS 5

// Definicao do registro
/* <<< DECLARE O REGISTRO AQUI >>> */
typedef struct _datas
{
	int dia;
	int mes;
	int ano;
	int estacao;
}Data;

// declaracoes das funcoes
/* <<< DECLARE OS PROTOTIPOS DAS FUNCOES AQUI >>> */
void alimentaData( Data *datas, int dias);
void descobreEstacao(Data datas, int estacoes[]);
/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main() {

   Data datas[TOTAL_DATAS]; // vetor para armazenar as datas
   int estacoes[4]; // vetor para armazenar quantidade de datas por estacoes
   
   int dias = 0, // quantidade de dias que serao convertidos
       i = 0;

   // inicializa registros
   for (i=0; i < TOTAL_DATAS; i++) {
      datas[i].ano = 0;
      datas[i].mes = 0;
      datas[i].dia = 0;
      datas[i].estacao = 0;
   }

   // inicializa contador das estacoes
   estacoes[PRIMAVERA] = 0;
   estacoes[OUTONO] = 0;
   estacoes[VERAO] = 0;
   estacoes[INVERNO] = 0;
   
   // le os dias de entrada e converte para registros de data
   for (i=0; i < TOTAL_DATAS; i++) {
      scanf("%d",&dias);
      alimentaData(&datas[i],dias); // recebe a qtde de dias e converte para data dd/mm/aaaa a partir da data 01/01/0001
      printf("%02d/%02d/%04d\n", datas[i].dia, datas[i].mes, datas[i].ano); // imprime a data no formato dd/mm/aaaa
      descobreEstacao(datas[i],estacoes); // descobre a estacao da data informada
   }

   // imprime quantas datas ficaram em cada estacao do ano
   printf("Primavera: %d\n", estacoes[PRIMAVERA]);
   printf("Outono: %d\n", estacoes[OUTONO]);
   printf("Verao: %d\n", estacoes[VERAO]);
   printf("Inverno: %d\n", estacoes[INVERNO]);

   return (0);

}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */
void alimentaData( Data *datas, int dias)
{
	int meses[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
	int mes = 0, ano = 1;
	
	while(dias > meses[mes])
	{
		dias = dias - meses[mes];
		if (dias < 0)//colocar em módulo
			dias = (-1)*dias;
		mes++;
		if (mes > 11)//trocou de ano
		{	
			mes = 0;
			ano++;
		}
	}
	if (dias < 0)//colocar em módulo
			dias = (-1)*dias;
	mes++;//porque no vetor vai de 0 a 11 e tem que ser de 1 a 12
	datas->dia = dias;
	datas->mes = mes;
	datas->ano = ano;
}
void descobreEstacao(Data datas, int estacoes[])
{
	if ((datas.dia >= 20 && datas.dia <= 31 && datas.mes == 3) || (datas.dia <= 20 && datas.mes == 6) || datas.mes == 4 || datas.mes == 5)
		estacoes[OUTONO]++;
	else
	
		if ((datas.dia >= 21 && datas.dia <= 30 && datas.mes == 6) || (datas.dia <= 22 && datas.mes == 9) || datas.mes == 7 || datas.mes == 8)
			estacoes[INVERNO]++;
		else
		
			if ((datas.dia >= 23 && datas.dia <= 30 && datas.mes == 9) || (datas.dia <= 21 && datas.mes == 12) || datas.mes == 10 || datas.mes == 11)
				estacoes[PRIMAVERA]++;
		else
		
				estacoes[VERAO]++;
}

