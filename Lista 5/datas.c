#include <stdio.h>

#define MAX 10

typedef unsigned int natural;

typedef struct _data
{
	natural dia;
	natural mes;
	natural ano;
}data;

void receberData(data *Data);
void imprimirData(data *Data);
data *maisAntiga(data *data1, data *data2);

int main()
{
	data vDatas[MAX], *p;
	natural qtDatas;
	int i;

	do
	{
		scanf ("%d",&qtDatas);
	}while ((qtDatas < 1) || (qtDatas > MAX));

	p = &vDatas[0];
	for (i=0; i < qtDatas; i++)
		receberData(p++);
	
	p = &vDatas[0];
	for (i=0; i < qtDatas; i++)
		p = maisAntiga(p, &vDatas[i]);

	printf("\nData mais antiga: ");
	imprimirData(p);

	
	return(0);
}

void receberData(data *Data)
{
	scanf("%u/%u/%u", &Data->dia, &Data->mes, &Data->ano);
}
void imprimirData(data *Data)
{
	printf("%02u/%02u/%02u\n", Data->dia, Data->mes, Data->ano);
}
data *maisAntiga(data *data1, data *data2)
{
	if ((data2->ano < data1->ano) || (data2->ano == data1->ano && data2->mes < data1->mes) || (data2->ano == data1->ano && data2->mes == data1->mes && data2->dia < data1->dia))	return (data2);
		return (data1);
}

