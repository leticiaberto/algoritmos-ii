#include <stdio.h>
#define MAX 10

void ler_vetor(float vet[],int n);
float calculo (float const vet[], int n, float *maior, float *menor);
int main()
{
	float vet[MAX], maior=0, menor=0, media=0;
	int n;
	
	scanf("%d",&n);

	ler_vetor(vet,n);
	media = calculo (vet, n, &maior, &menor);

	printf ("Média: %.2f\n",media);
	printf ("Maior nota: %.2f\n",maior);
	printf ("Menor nota: %.2f\n",menor);
	
	return(0);
}

void ler_vetor(float vet[],int n)
{
	float *p;
	for (p = vet; p < vet + n; p++)
		scanf ("%f",p);
}

float calculo (float const vet[], int n, float *maior, float *menor)
{
	const float *p;
	float s = 0;
	int i;
	*menor =  *vet;
	*maior = *vet;

	for (p = vet; p < vet + n; p++)
	{
		s = s + *p;
		if (*maior < *p)
			*maior = *p;
		if (*menor > *p)
			*menor = *p;
	
		printf ("%.2f ",*p);
	}
		
	return (s/n);
}
