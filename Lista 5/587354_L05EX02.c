/* ================================================================== *
   Universidade Federal de Sao Carlos - UFSCar, Sorocaba

   Disciplina: Algoritmos e Programação 2
   Prof. Tiago A. Almeida

   Lista 05 - Exercício 02 - Casa de Cambio

   Instrucoes
   ----------

   Este arquivo contem o codigo que auxiliara no desenvolvimento do
   exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
   Dados do aluno:

   RA: 587354
   Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

// Definicao do registro
/* <<< DECLARE O REGISTRO AQUI >>> */
typedef struct _cambio
{
	float *valorOriginal;
	float *txCambio;
	float valorConvertido;
	int nota100;
	int nota50;
	int nota20;
	int nota10;
	int nota5;
	int nota2;
	int moeda1real;
	int moeda50;
	int moeda25;
	int moeda10;
	int moeda5;
	int moeda1;
}Cambio;

// declaracoes das funcoes
/* <<< DECLARE OS PROTOTIPOS DAS FUNCOES AQUI >>> */
Cambio calculaCambio(float *valor, float *txCambio);

/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main() {

   Cambio conversao; // variavel para armazenar o resultado da conversao
   float valor, txCambio; // valores de entrada para calcular o cambio
   valor = 0.00;
   txCambio = 0.00;

   scanf("%f %f", &valor, &txCambio);
  
   conversao = calculaCambio(&valor, &txCambio);

   printf("Valor: %.2f\n",*conversao.valorOriginal);
   printf("Cambio: %.2f\n",*conversao.txCambio);
   printf("Total: %.2f\n",conversao.valorConvertido);
   if (conversao.nota100 > 0) printf("100,00: %d\n",conversao.nota100);
   if (conversao.nota50 > 0) printf("50,00: %d\n",conversao.nota50);
   if (conversao.nota20 > 0) printf("20,00: %d\n",conversao.nota20);
   if (conversao.nota10 > 0) printf("10,00: %d\n",conversao.nota10);
   if (conversao.nota5 > 0) printf("5,00: %d\n",conversao.nota5);
   if (conversao.nota2 > 0) printf("2,00: %d\n",conversao.nota2);
   if (conversao.moeda1real > 0) printf("1,00: %d\n",conversao.moeda1real);
   if (conversao.moeda50 > 0) printf("0,50: %d\n",conversao.moeda50);
   if (conversao.moeda25 > 0) printf("0,25: %d\n",conversao.moeda25);
   if (conversao.moeda10 > 0) printf("0,10: %d\n",conversao.moeda10);
   if (conversao.moeda5 > 0) printf("0,05: %d\n",conversao.moeda5);
   if (conversao.moeda1 > 0) printf("0,01: %d\n",conversao.moeda1);

   return 0;

}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */
Cambio calculaCambio(float *valor, float *txCambio)
{
	Cambio novo;
	float aux;
	int parteFracionaria, parteInteira;

	novo.valorOriginal = valor;
	novo.txCambio = txCambio;
	novo.valorConvertido = *valor * *txCambio;

	aux = novo.valorConvertido;

	//zerando as cedulas 

	novo.nota100 = 0;
	novo.nota50 = 0;
	novo.nota20 = 0;
	novo.nota10 = 0;
	novo.nota5 = 0;
	novo.nota2 = 0;
	novo.moeda1real = 0;
	novo.moeda50 = 0;
	novo.moeda25 = 0;
	novo.moeda10 = 0;
	novo.moeda5 = 0;
	novo.moeda1 = 0;
	
	/*fazer a contagem de cédulas*/

	// Pega a parte inteira do valor para calcular as notas
        parteInteira = (int) aux;

       /* Pega a parte fracionária e multiplica por 100, pq ai fica duas casa decimais para os centavos */
       
	parteFracionaria = (aux - parteInteira) * 100;

	//OBS: para pegar o menor numero de cédulas começamos a verificação pelo maior valor possivel, pois assim ja podemos eliminar os valores mais baixos desnecessarios

	/* Converter a parte inteira em notas */
	do
	{
		if(parteInteira>=100)
		{
			parteInteira-=100;
			novo.nota100++;
            	}
           	else 
			if(parteInteira>=50)
            		{
			     parteInteira-=50;
			     novo.nota50++;
            		}
            		else 
				if(parteInteira>=20)
			    	{
				     parteInteira-=20;
				     novo.nota20++;
		    		}
            			else 
					if(parteInteira>=10)
					{
					     parteInteira-=10;
					     novo.nota10++;
					}
            				else 
						if(parteInteira>=5)
		   				{
						     parteInteira-=5;
						     novo.nota5++;
          					}
           					else
							if(parteInteira>=2)
		    					{
							     parteInteira-=2;
							     novo.nota2++;
            						}
							else
								if(parteInteira==1)// 1 real
		    						{
							 		parteInteira-=1;
							  		novo.moeda1real++;
            							}

	}while(parteInteira!=0);

	//printf("parte frac: %df",parteFracionaria);
    	//Converter a parte fracionária em moedas(centavos)
	do
	{
		if(parteFracionaria>=50)
		{
			parteFracionaria-=50;
			novo.moeda50++;
		}
            	else 
			if(parteFracionaria>=25)
           		{
				parteFracionaria-=25;
            			novo.moeda25++;
            		}
            		else 
				if(parteFracionaria>=10)
            			{
					parteFracionaria-=10;
					novo.moeda10++;
            			}
           			else
					if(parteFracionaria>=5)
            				{
						parteFracionaria-=5;
						novo.moeda5++;
           				}
            				else 
						if(parteFracionaria>=1)//1 centavo
            					{
             						parteFracionaria-=1;
             						novo.moeda1++;
            					}
    	}while(parteFracionaria!=0);
	
	//retorna todos os campos
	return(novo);
}
