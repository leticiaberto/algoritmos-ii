/* ================================================================== *
   Universidade Federal de Sao Carlos - UFSCar, Sorocaba

   Disciplina: Algoritmos e Programação 2
   Prof. Tiago A. Almeida

   Lista 07 - Exercício 01 - Polinomio

   Instrucoes
   ----------

   Este arquivo contem o codigo que auxiliara no desenvolvimento do
   exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
   Dados do aluno:

   RA: 587354
   Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Opcoes ***********************
#define SAIR			0
#define ADICIONAR_POLI		1
#define REMOVER_POLI		2
#define SOMAR_POLI		3
#define SUBTRAIR_POLI		4
#define MULTIPLICAR_POLI	5
#define CALCULAR_POLI           6

// declaracoes de registros
/* <<< DEFINA OS REGISTROS AQUI >>> */
typedef struct _polinomio
{
	int coef;
	int exp;
	struct _polinomio *prox;
}Polinomio;
// declaracoes das funcoes
/* <<< DECLARE OS PROTOTIPOS DAS FUNCOES AQUI >>> */
void imprimir(Polinomio *p);
void adicionarPolinomio(Polinomio **p, int coef, int exp);
void removerPolinomio(Polinomio **p, int exp);
Polinomio *somarPolinomios(Polinomio *p1, Polinomio *p2);
Polinomio *subtrairPolinomios(Polinomio *polop1, Polinomio *polop2);
Polinomio *multiplicarPolinomios(Polinomio *p1,Polinomio *p2);
int calcularPolinomio(Polinomio *pol, int x);
void libera(Polinomio *resultado);
/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main() {

   int x, coef, exp, poliEscolhido, operando1, operando2;
   Polinomio *p1 = NULL;
   Polinomio *p2 = NULL;
   Polinomio *resultado = NULL;

   scanf("%d",&x);

   int opcao = 1;
   while(opcao != 0){
      scanf("%d",&opcao);
      switch(opcao){

         case ADICIONAR_POLI:
            scanf("%d %d %d", &poliEscolhido, &coef, &exp);
            poliEscolhido == 1 ? adicionarPolinomio(&p1,coef,exp) : adicionarPolinomio(&p2,coef,exp);
            //imprimir(p1);
         break;

         case REMOVER_POLI:
            scanf("%d %d", &poliEscolhido, &exp);
            poliEscolhido == 1 ? removerPolinomio(&p1,exp) : removerPolinomio(&p2,exp);
         break;

         case SOMAR_POLI:
            if(resultado != NULL) libera(resultado);
            resultado = somarPolinomios(p1,p2);
           
         break;

         case SUBTRAIR_POLI:
            scanf("%d %d", &operando1, &operando2);
            if(resultado != NULL) libera(resultado);
            resultado = subtrairPolinomios(operando1 == 1 ? p1 : p2, operando2 == 2 ? p2 : p1);
         break;

         case MULTIPLICAR_POLI:
            if(resultado != NULL) libera(resultado);
            resultado = multiplicarPolinomios(p1,p2);
           //imprimir(resultado);
         break;

         case CALCULAR_POLI:
            scanf("%d",&poliEscolhido);
            switch(poliEscolhido) {
               case 1: printf("%d\n",calcularPolinomio(p1,x)); break;
               case 2: printf("%d\n",calcularPolinomio(p2,x)); break;
               case 3: printf("%d\n",calcularPolinomio(resultado,x)); break;
            }
         break;
	
	
	 
      
   }
}
   return (0);

}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */
void libera(Polinomio *resultado)
{
	Polinomio *apaga,*aux = resultado;
	while (aux!= NULL)
	{
		apaga = aux;
		aux = aux->prox;
		free (apaga);	
	}
}
void adicionarPolinomio(Polinomio **p, int coef, int exp)
{
		
	if ((*p) == NULL || (*p)->exp > exp)
	{	
		Polinomio *novo = (Polinomio *) malloc (sizeof(Polinomio));//aloca na memoria
		novo->coef = coef;
		novo->exp = exp;
		novo->prox= *p;
		*p = novo;
		
	} else if ((*p)->exp == exp)
	{
		(*p)->coef = (*p)->coef + coef;
	}
	else
	{	
		Polinomio *aux = *p;
		while (aux->prox != NULL && aux->prox->exp < exp)//insere ordenado
		{	
			aux = aux->prox;
		}
		
		if (aux->prox != NULL && aux->prox->exp == exp)
		{
			aux->prox->coef = aux->prox->coef + coef;
		}
		else
		{
			Polinomio *novo = (Polinomio *) malloc (sizeof(Polinomio));//aloca na memoria
			novo->coef = coef;
			novo->exp = exp;
			novo->prox= aux->prox;
			aux->prox = novo;
		}
		
	}
}
void removerPolinomio(Polinomio **p, int exp)
{
	Polinomio *aux = *p, *apaga;
	if ( (*p) != NULL && (*p)->exp == exp)//testa o primeiro elemento //ou aux->prox == NULL?
	{	
		apaga = aux;
		aux = aux->prox;
		free(apaga);
	}
	else
	{
		while (aux->prox != NULL && aux->prox->exp != exp)
			aux = aux->prox;
		
		if (aux->prox != NULL && aux->prox->exp == exp)
		{
			apaga = aux->prox;
			aux->prox = aux->prox->prox;
			free(apaga);
		}else
		if (aux->prox->prox == NULL && aux->prox->exp == exp)
		{
			apaga = aux->prox;
			aux->prox = NULL;
			free(apaga);
		}			
	}//imprimir(aux);	
}
Polinomio *somarPolinomios(Polinomio *p1, Polinomio *p2)
{
	Polinomio *soma=NULL;
	
	while (p1 != NULL) {
		adicionarPolinomio(&soma, p1->coef, p1->exp);
		p1 = p1->prox;
	}
	while (p2 != NULL) {
		adicionarPolinomio(&soma, p2->coef, p2->exp);
		p2 = p2->prox;	
	}
	
	return soma;
}
Polinomio *subtrairPolinomios(Polinomio *polop1, Polinomio *polop2)
{
	Polinomio *sub=NULL;
	Polinomio *aux = sub, *apaga;
	while (polop1 != NULL) {
		adicionarPolinomio(&sub, polop1->coef, polop1->exp);
		polop1 = polop1->prox;
	}
	while (polop2 != NULL) {
		adicionarPolinomio(&sub, (polop2->coef)*-1, polop2->exp);
		polop2 = polop2->prox;	
	}
	/*if (sub->coef == 0)//testa primeiro elemento 
	{		
			apaga = sub;
			sub->prox = sub->prox;
			free(apaga);
		
	}
	while (aux->prox != NULL || aux->prox == NULL)
	{		
		if (aux->prox->coef == 0)
		{
			apaga = aux->prox;
			aux->prox = aux->prox->prox;
			free(apaga);
		}
		aux = aux->prox;
	}	*/
	
	return sub;
}
Polinomio *multiplicarPolinomios(Polinomio *p1,Polinomio *p2)
{
	Polinomio *mult = NULL;
	
	while (p1 != NULL)
	{ Polinomio *auxp2 = p2;
		while (auxp2 != NULL)
		{
			//mult->coef = p1->coef * p2->coef;
			//mult->exp = p1->exp + p2->exp;
			adicionarPolinomio(&mult, p1->coef * auxp2->coef, p1->exp + auxp2->exp);
			auxp2 = auxp2->prox;
		}
	p1 = p1->prox;
	}
	return mult;
}
int calcularPolinomio(Polinomio *pol, int x)
{
	int result=0;
	while (pol != NULL) {
	result = result + pol->coef * pow(x,pol->exp);
	//printf("result: %d\n",result);
	pol = pol->prox;
	}
	return (result);
}

void imprimir(Polinomio *p) {
	while (p != NULL) {
  		printf("%d - %d\n", p->coef, p->exp);
 	 	p = p->prox;
	}

}
