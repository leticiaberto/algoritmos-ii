/* ================================================================== *
   Universidade Federal de Sao Carlos - UFSCar, Sorocaba

   Disciplina: Algoritmos e Programação 2
   Prof. Tiago A. Almeida

   Lista 07 - Exercício 02 - Fila e Pilha

   Instrucoes
   ----------

   Este arquivo contem o codigo que auxiliara no desenvolvimento do
   exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
   Dados do aluno:

   RA: 587354
   Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Estruturas *********************
#define FILA			100
#define PILHA			200

// Opcoes ***********************
#define SAIR			0
#define ADICIONAR		1
#define REMOVER			2
#define PROCURAR		3
#define CALCULAR_TAMANHO	4
#define IMPRIMIR		5

// declaracoes de registros
/* <<< DEFINA OS REGISTROS AQUI >>> */
typedef struct registro {
   int valor;
   struct registro* prox;
} Registro;

// declaracoes das funcoes
/* <<< DECLARE OS PROTOTIPOS DAS FUNCOES AQUI >>> */
Registro *adicionarElementoFila(Registro *fila, int valor);
Registro *adicionarElementoPilha(Registro *pilha, int valor);
Registro *removerElementoFila(Registro *fila);
Registro *removerElementoPilha(Registro *pilha);
int procurarElemento(Registro *estrutura, int valor);
int calcularTamanho(Registro *estrutura);
void imprimir(Registro *estrutura);
void libera(Registro *estrutura);
/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main() {

   int valor, estrutura;
   Registro *pilha = NULL;
   Registro *fila = NULL;

   int opcao = 1;
   while(opcao != 0){
      scanf("%d",&opcao);
      switch(opcao){

         case ADICIONAR:
            scanf("%d %d", &estrutura, &valor);
            if(estrutura == 100)
               fila = adicionarElementoFila(fila, valor);
            else
               pilha = adicionarElementoPilha(pilha, valor);
         break;

         case REMOVER:
            scanf("%d", &estrutura);
            if(estrutura == 100)
               fila = removerElementoFila(fila);
            else
               pilha = removerElementoPilha(pilha);
         break;

         case PROCURAR:
            scanf("%d %d", &estrutura, &valor);
            printf("%d\n", procurarElemento(estrutura == 100 ? fila : pilha, valor));
         break;

         case CALCULAR_TAMANHO:
            scanf("%d", &estrutura);
            printf("%d\n", calcularTamanho(estrutura == 100 ? fila : pilha));
         break;

         case IMPRIMIR:
            scanf("%d", &estrutura);
            imprimir(estrutura == 100 ? fila : pilha);
         break;

      }
   }

   libera(fila);
   libera(pilha);

   return (0);

}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */
// verifica se inicio da lista é NULL

Registro *adicionarElementoFila(Registro *fila, int valor)
{
	//cria um novo nó
  	Registro *pnovo = (Registro *)malloc(sizeof(Registro));

 	//verifica se a memória foi alocada
    pnovo->valor = valor; //preenche os dados
   	pnovo->prox = NULL; //define que o próximo é nulo
   	Registro *aux = fila;
   	
	if (fila == NULL)
		fila = pnovo;
	else{
   	 while (aux->prox != NULL)//percorre a fila
		aux = aux->prox;
		
	aux->prox = pnovo;
	}
	return fila;
}
Registro *adicionarElementoPilha(Registro *pilha, int valor)
{
  	Registro *pnovo = (Registro *)malloc(sizeof(Registro));
	Registro *aux = pilha;

    pnovo->valor = valor; 
   	
	if (pilha == NULL)
	{	pnovo->prox = NULL;
		pilha = pnovo;
	}	
	else{
   	    pnovo->prox = pilha; 
		pilha = pnovo;
	}
	  //imprimir (pilha); 
	return pilha;
}
Registro *removerElementoFila(Registro *fila)
{
	Registro *apaga;
	apaga = fila;
	fila = fila->prox;
	free(apaga);
	return fila;
}
Registro *removerElementoPilha(Registro *pilha)
{
	Registro *apaga;
	Registro *aux = pilha;
	
	apaga = pilha;
	pilha= pilha->prox;
	free(apaga);
	return pilha;
}
int procurarElemento(Registro *estrutura, int valor)
{
	int flag = -1,pos = -1;
	while (estrutura != NULL) {
		pos++;
      if (estrutura->valor == valor)
      	flag = pos;
      estrutura = estrutura->prox;
    }
   
    return flag;
}
int calcularTamanho(Registro *estrutura)
{
	int cont=0;
	while (estrutura != NULL)
	{	cont++;
		estrutura = estrutura->prox;
	}
	return cont;
}
void imprimir(Registro *estrutura)
{  
	int flag = 0;
   while (estrutura != NULL) {
      printf("%d ", estrutura->valor);
      estrutura = estrutura->prox;
      flag = 1;
   }
   if (flag == 1)
 	printf("\n");
}
void libera(Registro *estrutura)
{
	Registro *apaga,*aux = estrutura;
	while (aux!= NULL)
	{
		apaga = aux;
		aux = aux->prox;
		free (apaga);	
	}
}
//libera ok
//tamanho ok
//imprimir ok
//busca ok
//adicionar fila ok
//remover fila ok
