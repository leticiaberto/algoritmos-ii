/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 2
	Prof. Tiago A. Almeida

	Lista 02 - Exercício 02 - Cadastro de alunos

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354	
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// menu de opcoes
#define INSERIR 			1
#define ALTERAR 			2
#define REMOVER 			3
#define BUSCAR 				4
#define LISTAR				5
#define LISTARPORNOME		6
#define SAIR 				0

// frases pre-definidas para saida
#define FRASE_ALUNO_NAO_ENCONTRADO "Cadastro nao encontrado!\n"
#define FRASE_CADASTRO_CHEIO "O limite de alunos foi atingido!\n"
#define FRASE_CADASTRO_VAZIO "Nao ha nenhum aluno cadastrado!\n"
#define FRASE_JACADASTRADO "Aluno ja cadastrado!\n"

// Limitantes
#define TAM_NOME 1000	// tamamnho maximo do nome do aluno
#define MAX_ALUNOS 100 	// quantidade maxima de alunos


// Definicoes dos registros para armazenar dados de um aluno
/* <<< DEFINA OS REGISTROS AQUI >>> */
typedef struct _data{
	int dia;
	int mes;
	int ano;
}data;

typedef struct _aluno{
	int ra;
	int creditos;
	char nome[TAM_NOME+1];
	data dataIngresso;
}aluno;

//Declaracoes das funcoes
void ordenarCrescente(aluno Alunos[], int qtAlunos);
int inserir (aluno Alunos[], int qtAlunos, aluno novoAluno);
void listar (aluno Alunos[], int qtAlunos);
int alterar(aluno Alunos[], int qtAlunos, int ra);
int remover(aluno Alunos[], int qtAlunos, int ra);
void listarPorNome(aluno Alunos[], int qtAlunos);
int buscar(aluno Alunos[], int qtAlunos, int ra);
/* <<< ESCREVA OS PROTOTIPOS DAS DEMAIS FUNCOES AQUI >>> */


/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main(){
	
	int opcao;
	int qtAlunos=0, indice, retorno;
	int ra;
	aluno Alunos[MAX_ALUNOS];
	aluno novoAluno;
	
	do {
		scanf("%d",&opcao);

		switch(opcao){

			case INSERIR:
				scanf("%d %d/%d/%d %d %[^\n]s", &(novoAluno.ra), 
				&(novoAluno.dataIngresso.dia), &(novoAluno.dataIngresso.mes), &(novoAluno.dataIngresso.ano),
				&(novoAluno.creditos),(novoAluno.nome));

				retorno = inserir(Alunos, qtAlunos, novoAluno);
				if (retorno == 1)
					qtAlunos++;
				else if (retorno == -1)
					printf(FRASE_CADASTRO_CHEIO);
				else 
					printf(FRASE_JACADASTRADO);

				break;


			case ALTERAR:
				scanf("%d",&ra);

				if (alterar(Alunos, qtAlunos, ra) == 0)
					printf(FRASE_ALUNO_NAO_ENCONTRADO);

				break;


			case REMOVER:
				scanf("%d",&ra);

				if (remover(Alunos, qtAlunos, ra))
					qtAlunos--;
				else
					printf(FRASE_ALUNO_NAO_ENCONTRADO);

				break;


			case BUSCAR:
				scanf("%d",&ra);

				indice = (buscar(Alunos, qtAlunos, ra));

				if (indice != -1){
					printf("%06d - ",Alunos[indice].ra);
					printf("%s - ",Alunos[indice].nome);
					printf("%02d/%02d/%04d - ",Alunos[indice].dataIngresso.dia, Alunos[indice].dataIngresso.mes, Alunos[indice].dataIngresso.ano);
					printf("%04d\n",Alunos[indice].creditos);
				} else
					printf(FRASE_ALUNO_NAO_ENCONTRADO);

				break;


			case LISTAR:

				if (qtAlunos > 0)
					listar(Alunos, qtAlunos);
				else
					printf(FRASE_CADASTRO_VAZIO);
				break;


			case LISTARPORNOME:

				if (qtAlunos > 0)
					listarPorNome(Alunos, qtAlunos);
				else
					printf(FRASE_CADASTRO_VAZIO);
				break;

			
		}	
	} while(opcao != 0);
	
	
	return (0);

}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */
void ordenarCrescente(aluno Alunos[], int qtAlunos)//Metodo Bolha
{
      int j, i;
      aluno novoAluno;
      for(i = 0; i < qtAlunos; i++)
      {
         for(j=i+1; j < qtAlunos ; j++)
	 {
            if(Alunos[i].ra > Alunos[j].ra)
	    { novoAluno =  Alunos[i];
		 Alunos[i] =  Alunos[j];
		 Alunos[j] = novoAluno;
            }
         }
      }  
}
// listar(): lista todos os registros em ordem crescente de RA
void listar (aluno Alunos[], int qtAlunos){
	int i=0;

	ordenarCrescente(Alunos,qtAlunos);

	for (i=0; i<qtAlunos; i++){
		printf("%06d - ",Alunos[i].ra);
		printf("%s - ",Alunos[i].nome);
		printf("%02d/%02d/%04d - ",Alunos[i].dataIngresso.dia, Alunos[i].dataIngresso.mes, Alunos[i].dataIngresso.ano);
		printf("%04d\n",Alunos[i].creditos);
	}
}


/* <<< IMPLEMENTE AS DEMAIS FUNCOES AQUI >>> */
int inserir (aluno Alunos[], int qtAlunos, aluno novoAluno)
{
	int i=0,flag=1;

	if (qtAlunos == MAX_ALUNOS)
		flag = -1;
	for (i=0;i<qtAlunos;i++)
	{
		if (novoAluno.ra == Alunos[i].ra)
			flag = 0;
	}
	if (flag == 1)
	{	Alunos[qtAlunos] = novoAluno;
	}
	return(flag);
// inserir(): retorna 1 se o cadastro foi efetuado com sucesso, -1 caso não haja espaço suficiente ou 0 caso
// o RA já esteja cadastrado
}

int alterar(aluno Alunos[], int qtAlunos, int ra)
{
	int flag=0,i;
	aluno novoAluno;
	for (i=0;i<qtAlunos;i++)
	{
		if (Alunos[i].ra == ra)
		{
			flag = 1;
			novoAluno.ra = ra;
			scanf("%d/%d/%d %d %[^\n]s", 
				&(novoAluno.dataIngresso.dia), &(novoAluno.dataIngresso.mes), &(novoAluno.dataIngresso.ano),
				&(novoAluno.creditos),(novoAluno.nome));
			Alunos[i] = novoAluno;
		}
	}
	return (flag);
// alterar(): retorna 1 se a alteracao foi efetuada com sucesso ou 0 caso não o registro não tenha sido encontrado
}
int remover(aluno Alunos[], int qtAlunos, int ra)
{
	int i,j,elementos,flag = 0,aux=0;
	aluno AlunoNovo[MAX_ALUNOS];

	elementos = qtAlunos ;//ao remover um novo elemento, a quantidade diminui em 1

	for (i=0;i < elementos;i++)
	{
		if ((Alunos[i].ra) == ra)//a posição no vetor começa no zero, mas o usuario digita a partir do um, logo é necessario colocar i+1 para manipular as posições de acordo com a contagem do usuario
		{
			for (j=i;j < elementos-1; j++)//deslocamos os numeros para a esquerda com o comendo j=i
			{
				AlunoNovo[j] = Alunos[j+1];
				
			}
			//listanova[i] = valor;
			flag = 1;
			aux=1;//flag de retorno da função
		}
		else
			if (flag==0)//flag=0 indica que a posição ainda nao foi encontrada, logo populamos o novo vetor de acordo com a ordem do Lista[]
			{	
				AlunoNovo[i] = Alunos[i];
			}
	}
	for (i=0;i< elementos-1;i++)//joga os valores do novo vetor para o vetor Lista[] para poder fazer a impressao correta
	{
		Alunos[i] = AlunoNovo[i];
		
	}
	return (aux);
// remover(): retorna 1 se a remocao foi efetuada com sucesso ou 0 caso não o registro não tenha sido encontrado
}
void listarPorNome(aluno Alunos[], int qtAlunos)
{
	aluno NovoAluno;
     int i,j;
       
   for ( i = 0; i < qtAlunos-1; i++)  
     {       for ( j = i + 1; j < qtAlunos; j++)  
              {  if (strcmp(Alunos[i].nome,Alunos[j].nome) > 0) {  
                    NovoAluno = Alunos[i];  
                    Alunos[i] = Alunos[j];  
                    Alunos[j]= NovoAluno;  
                } }  }
	//impressão
	for (i=0; i<qtAlunos; i++){
		printf("%06d - ",Alunos[i].ra);
		printf("%s - ",Alunos[i].nome);
		printf("%02d/%02d/%04d - ",Alunos[i].dataIngresso.dia, Alunos[i].dataIngresso.mes, Alunos[i].dataIngresso.ano);
		printf("%04d\n",Alunos[i].creditos);}
	
// listarPorNome(): lista todos os registros em ordem alfabetica
}
int buscar(aluno Alunos[], int qtAlunos, int ra)
{
	int flag=-1,i;
	for (i=0;i<qtAlunos;i++)
	{
		if(Alunos[i].ra == ra)
			flag = i;
	}
return (flag);
// buscar(): retorna o índice do registro caso ele tenha sido encontrado ou -1 caso contrario
}




