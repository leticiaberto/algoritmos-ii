/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 2
	Prof. Tiago A. Almeida

	Lista 02 - Exercício 01 - Caça-palavras

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <string.h>

int dp=0;//variavel global para controlar o caso da vertical e diagonal principal

/* máximo de linhas (m) e colunas (n) */
#define MAX 10

/* constantes booleanas */	
#define VERDADEIRO	1
#define FALSO		0


/* Protótipos das funções */
void lerGrade(char grade[][MAX], int m, int n);
void imprimirGrade(char grade[][MAX], int m, int n);
/* <<< ESCREVA OS PROTOTIPOS DAS DEMAIS FUNCOES AQUI >>> */
void esconderMarcas(char grade[][MAX], int marcas[MAX][MAX], int m, int n);
int procurarHorizontal(char palavra[MAX], char grade[][MAX], int marcas[MAX][MAX],int m,int n);
int procurarVertical(char palavra[MAX], char grade[][MAX], int marcas[MAX][MAX], int m, int n);
int procurarDPrincipal(char palavra[MAX], char grade[][MAX], int marcas[MAX][MAX], int m, int n);
int procurarDSecundaria(char palavra[MAX], char grade[][MAX], int marcas[MAX][MAX],int m,int n);
void inverterString(char palavra[MAX]);

/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */

int main(void)
{
	char grade[MAX][MAX]; 	/* matriz de letras do caça-palavras */
	int marcas[MAX][MAX]; 	/* cada posição vale 1 se é para esconder a letra na resposta, ou 0 caso contrário */
	int num_palavras;		/* qtde de palavras que usuário deseja procurar na grade */
	char palavra[MAX];		/* palavra que o usuário deseja buscar */
	int m, n;				/* dimensões da grade de letras */ 
	int i, j;				/* iteradores para percorrer linhas e colunas */
	int achou;				/* variavel que indica se palavra foi encontrada na grade (achou=1) ou não (achou=0)*/
	

	/* lê dimensão e a grade */
	scanf("%d %d", &m, &n);
	lerGrade(grade, m, n);

	
	/* marca todas as posições da grade como fora da resposta final */
	for (i = 0; i < m; i++){
		for (j = 0; j < n; j++){
			marcas[i][j] = 1;
		}
	}


	/* lê número de palavras e procura cada uma delas na grade */
	scanf("%d", &num_palavras);

	/* para cada palavra desejada */
	for(i = 0; i < num_palavras; i++) {
		scanf("%s", palavra); // le a palavra desejada
		
		// procura em todas as linhas da grade
		achou = procurarHorizontal(palavra, grade, marcas, m, n);

		if (achou == FALSO) {
		
			// procura em todas as colunas da grade
			achou = procurarVertical(palavra, grade, marcas, m, n);
		
			if (achou == FALSO) {
				
				// procura na diagonal principal
				achou = procurarDPrincipal(palavra, grade, marcas, m, n);

				if (achou == FALSO) {
					
					// procura na diagonal secundária
					achou = procurarDSecundaria(palavra, grade, marcas, m, n);
					if (achou == FALSO) {

						// Inverte a palavra e reinicias as buscas
						inverterString(palavra);

						// procura em todas as linhas da grade
						achou = procurarHorizontal(palavra, grade, marcas, m, n);

						if (achou == FALSO)	{

							// procura em todas as colunas da grade
							achou = procurarVertical(palavra, grade, marcas, m, n);

							if (achou == FALSO) {

								// procura na diagonal principal
								achou = procurarDPrincipal(palavra, grade, marcas, m, n);
								
								if (achou == FALSO) {

									// procura na diagonal secundária
									achou = procurarDSecundaria(palavra, grade, marcas, m, n);
								}
							}
						}
					}
				}
			}
		}
	}
	
	// coloca '#' na grade no lugar de letras não usadas
	esconderMarcas(grade, marcas, m, n);

	// imprime a grade final
	imprimirGrade(grade, m, n);
	
	return 0;
}


/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */

/* lerGrade: preenche grade lendo do teclado cada caractere */
void lerGrade(char grade[][MAX], int m, int n)
{
	int i, j;
	
	for (i = 0; i < m; i++)	{
		for (j = 0; j < n; j++) {
			do {
				scanf("%c", &grade[i][j]);
			/* ignora espaços e quebras de linha */
			} while (grade[i][j] == ' ' || grade[i][j] == '\n');
		}
	}
}


/* imprimirGrade: imprime a grade de caracteres */
void imprimirGrade(char grade[][MAX], int m, int n)
{
	int i, j;
	
	for (i = 0; i < m; i++) {
	 	for (j = 0; j < n; j++) {
			printf("%c ", grade[i][j]);
		}
		printf("\n");
	}
}

/* <<< IMPLEMENTE AS DEMAIS FUNCOES AQUI >>> */
void esconderMarcas(char grade[][MAX], int marcas[MAX][MAX], int m, int n)
{
	int i,j;
	for (i = 0; i < m; i++) 
	{
	 	for (j = 0; j < n; j++) 
		{
			if (marcas[i][j] != 0)
				grade[i][j] = '#';//vai aparecer na resposta final
		}
	}
}
int procurarHorizontal(char palavra[MAX], char grade[][MAX], int marcas[MAX][MAX],int m,int n)
{
	int flag=0,i,k=0,j,cont=0,jini,tampala=0,aux=0,qntd=0,passou=0,jfim;
	//define o tamanho da palavra
	for (k=0; palavra[k] != '\0';k++)
		tampala++;
	//printf ("%d\n:",tampala);
		for (i = 0; i < m; i++) 
		{
		 	for (j = 0; j < n; j++) 
			{
				if (grade[i][j] != palavra[aux] && passou==1)//Para quando encontrar uma palavra pela metade, cancela a desmarcação
						{	cont=0;
							aux=0;
							if (grade[i][j] == palavra[aux])
							{	aux++;
								passou=1;cont++;jini = j;
							}
						}
				if (grade[i][j] == palavra[aux] && passou!=0)
				{
					aux++;
					cont++;jfim=j;
				}
				if (grade[i][j] == palavra[aux] && passou==0)
				{
					aux++;
					cont++;jini = j;passou=1;
				}	if(aux==tampala)
						break;		
			}
			if (cont == tampala)
			{	for (k=jini;k<= jfim; k++)
					marcas[i][k] = 0;//printf ("Marcas: %d\n",marcas[i][k]);
				flag = 1;			
			}
			cont = 0;
			aux=0;
			passou=0;
		}			
	return (flag);
}
int procurarVertical(char palavra[MAX], char grade[][MAX], int marcas[MAX][MAX], int m, int n)
{
	int flag=0,i,k=0,j,cont=0,iini,tampala=0,aux=0,qntd=0,passou=0,ifim;
	//define o tamanho da palavra
	for (k=0; palavra[k] != '\0';k++)
		tampala++;
	//printf ("%d\n:",tampala);
		for (j = 0; j < n; j++) 
		{
		 	for (i = 0; i < m; i++)
			{	if (grade[i][j] != palavra[aux] && passou==1 && dp==1)//Para quando encontrar uma palavra pela metade, cancela a desmarcação e dp==1 para nao dar erro no caso da diagonal principal
				{	cont=0;
					aux=0;
					if (grade[i][j] == palavra[aux])
					{	aux++;
						passou=1;cont++;iini = i;
					}
				}
				if (grade[i][j] == palavra[aux] && passou!=0)
				{
					aux++;
					cont++;ifim=i;
				}
				if (grade[i][j] == palavra[aux] && passou==0)
				{
					aux++;
					cont++;iini = i;passou=1;
				}
			
			}
			if (cont == tampala)
			{	for (k=iini;k<= ifim; k++)
					marcas[k][j] = 0;//printf ("Marcas: %d\n",marcas[i][k]);
				flag = 1;			
			}
			cont = 0;
			aux=0;
			passou=0;
		}		
	return (flag);
}

int procurarDPrincipal(char palavra[MAX], char grade[][MAX], int marcas[MAX][MAX], int m, int n)
{
	int flag=0,i,k=0,j,cont=0,tampala=0,aux=0,qntd=0,passou=0, iini, ifim;

	if (m == n) 
	{
		//define o tamanho da palavra
		for (k=0; palavra[k] != '\0';k++)
			tampala++;
		//printf ("%d\n:",tampala);
			for (i = 0; i < m; i++) 
			{
			 	for (j = 0; j < n; j++) 
				{	
					if (i == j)
					{	if (grade[i][j] != palavra[aux] && passou==1)//Para quando encontrar uma palavra pela metade, cancela a desmarcação
						{	cont=0;
							aux=0;
							if (grade[i][j] == palavra[aux])
							{	aux++;
								passou=1;cont++;iini = i;
							}
						}
						if (grade[i][j] == palavra[aux] && passou!=0)
						{
							aux++;
							cont++;ifim=i;
						}
						if (grade[i][j] == palavra[aux] && passou==0)
						{
							aux++;
							cont++;passou=1;iini = i;
						}
						
					}			
				}
				if (cont == tampala)
				{	for (k=iini;k<= ifim; k++)
						marcas[k][k] = 0;//printf ("Marcas: %d\n",marcas[i][k]);
					flag = 1;		
				}
				
			}					
	}dp=1;//dp==1 para nao dar erro no caso da vertical
	return (flag);
} 
		
	 
int procurarDSecundaria(char palavra[MAX], char grade[][MAX], int marcas[MAX][MAX],int m,int n)
{
	int flag=0,i,k=0,j,cont=0,jini,tampala=0,aux=0,passou=0,jfim, iini, ifim, c = 0, p;

	if (m == n) 
	{
		//define o tamanho da palavra
		for (k=0; palavra[k] != '\0';k++)
			tampala++;
		//printf ("%d\n:",tampala);
			for (i = 0; i < m; i++) 
			{
			 	for (j = 0; j < n; j++) 
				{
					if (j == m - i -1)
					{//printf("j: %d\n",j);
						if (grade[i][j] == palavra[aux] && passou!=0)
						{
							aux++;
							cont++;jfim=j;ifim=i;
						}
						if (grade[i][j] == palavra[aux] && passou==0)
						{
							aux++;
							cont++;jini = j;passou=1;iini = i;
						}
					}				
				c++;
				}
			
				if (cont == tampala)
				{	for (k=iini,p=jini;k< ifim+1; k++,p--)
						marcas[k][p] = 0;//printf ("Marcas: %d\n",marcas[i][k]);
					flag = 1;			
				}
			}
	}
	return (flag);
}
void inverterString(char palavra[MAX])
{
	int i,k,tampala=0;
	char invertida[MAX];

	//define o tamanho da palavra
	for (k=0; palavra[k] != '\0';k++)
		tampala++;//printf("tam: %d\n",tampala);
	for(i=tampala-1,k=0; i>=0; i--,k++)
	{
		invertida[k] = palavra[i];//printf("palavra: %d\n",invertida[k]);
	}
	for (k=0; k<tampala;k++)
	{
		palavra[k] = invertida[k];
	}
}
/* inverterString: inverte a ordem da string, p. ex, troca "AMOR" por "ROMA" */

/* procurarHorizontal: procura a palavra nas linhas da grade. Remove as marcas, se a palavra for 
encontrada e retorna TRUE ou FALSE se a palavra foi encontrada ou nao */

/* procurarVertical: procura a palavra nas colunas da grade. Remove as marcas, se a palavra for 
encontrada e retorna TRUE ou FALSE se a palavra foi encontrada ou nao */

/* procurarDPrincipal: procura a palavra na diagonal principal da grade. Remove as marcas, se a palavra for 
encontrada e retorna TRUE ou FALSE se a palavra foi encontrada ou nao */

/* procurarDSecundaria: procura a palavra na diagonal secundaria da grade. Remove as marcas, se a palavra for 
encontrada e retorna TRUE ou FALSE se a palavra foi encontrada ou nao */
          
/* esconderMarcas: substitui caracteres da grade por '#' onde estiver marcado */



