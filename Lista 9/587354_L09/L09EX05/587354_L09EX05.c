/* Nome: Letícia Mara Berto
RA: 587354 */
#include <stdio.h>
#include <stdlib.h>
int gerarArquivoBinario(FILE *f1,int n,int m,char nome[]);
int lerImprimirArquivo(FILE *f1,char nome[]);
int salvarArquivoTexto(FILE *f1, FILE *f2, char nome1[], char nome2[]);
int main()
{
	FILE *f1, *f2;
	int n,m;
	char nome1[255], nome2[255];
	
	scanf("%[^\n]%*c",nome1);
	scanf("%[^\n]",nome2);
	scanf("%d %d", &n, &m);
		
	gerarArquivoBinario(f1,n,m,nome1);
	
	salvarArquivoTexto(f1, f2,nome1,nome2);
	lerImprimirArquivo(f2,nome2);
	return 0;
}
int gerarArquivoBinario(FILE *f1,int n,int m,char nome[])
{
	int i,j,d;
	//Gera arquivo binario
	f1 = fopen(nome, "wb");
	if (f1 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	fwrite(&n, sizeof(int), 1, f1);
	fwrite(&m, sizeof(int), 1, f1);
		
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{	
			d = (i*m) + j; 
			fwrite(&d, sizeof(int), 1, f1);	
		}
	}
	fclose(f1);
	//gerou arq bin
}
int salvarArquivoTexto(FILE *f1, FILE *f2, char nome1[], char nome2[])
{
	int *v1, j, i, n ,m;
	//Arquivo Binário
	f1 = fopen(nome1, "rb");
	if (f1 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	//Arquivo Texto
	f2 = fopen(nome2, "w");
	if (f2 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	fread(&n, sizeof(int), 1, f1); 
	fread(&m, sizeof(int), 1, f1);
 	v1 = (int *)malloc(n*m*sizeof(int)); // aloca a estrutura
 	fread(v1, sizeof(int), n*m, f1); 
 	
 	fprintf(f2, "%d %d\n", n, m); /* Dimensão da matriz */
  int cont = 0;
  for (i = 0; i < n*m; i++)
  {
  	if (cont == m)//apenas para representar na forma de matriz
  	{	fprintf(f2, "\n");
  		cont = 0;
  	}
  	fprintf(f2, "%d ", v1[i]);
    cont++;
  }
	free (v1);
	fclose(f1);
	fclose(f2);
}
int lerImprimirArquivo(FILE *f1,char nome[])
{
	char num;
	f1 = fopen (nome, "r");    

  	if (f1 == NULL)
  	{
		perror("Erro ao abrir o arquivo");
   		return 1;  
	}

		while (fscanf(f1, "%c", &num) != EOF)
		printf("%c", num);
	 printf("\n");
	fclose(f1);
}
