/* Nome: Letícia Mara Berto
RA: 587354 */
#include <stdio.h>
#include<stdlib.h>
typedef struct _valores
{
	char c;
	int inteiro;
	float real;
}Valores;
void ordenar(Valores *vet, int l, FILE *fr, char nome[]);
void ler(FILE *fr, char nome[]);
void imprimir(Valores *vet, int l, FILE *fr, char nome[]);
int texto(Valores *vet, int l, FILE *fr, char nome[]);
int main()
{
	FILE *fr;
	char nome[100], c;
	
	scanf("%[^\n]",nome);
	
	fr = fopen(nome, "rb");//só para ler
	if (fr == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	ler(fr,nome);
	
	return 0;
}
void ler(FILE *fr, char nome[])
{
	int l,i;
	
	///fscanf(fr, "%d\n", &l); //lê o numero de linhas
	fread(&l, sizeof(int), 1, fr); //lê o numero de linhas
	Valores *vet = (Valores *) malloc (l * sizeof(Valores));
	
	for (i = 0; i< l; i++)
	{
		//fscanf(fr, "%c %d %f\n",&vet[i].c, &vet[i].inteiro, &vet[i].real);//\n pra desconsiderar na leitura do arquivo
		fread(&vet[i].c, sizeof(char), 1, fr);
		fread(&vet[i].inteiro, sizeof(int), 1, fr);
		fread(&vet[i].real, sizeof(float), 1, fr);
	}	
	
	ordenar(vet,l,fr,nome);
}
void ordenar(Valores *vet, int l, FILE *fr, char nome[])
{
	Valores aux;
	int j, i;
       
      for(i = 0; i < l-1; i++)
      {
        for(j=i+1; j < l ; j++)
	 	{
            if(vet[i].c > vet[j].c)
	   		{
               aux = vet[i];
               vet[i] = vet[j];
               vet[j] = aux;
            }
         }
      }  
     fclose(fr);
     imprimir(vet,l,fr,nome);
}
void imprimir(Valores *vet, int l, FILE *fr, char nome[])
{
	int i;
	char num;
	fr = fopen(nome, "wb");//salvar as alterações
	fwrite(&l, sizeof(int), 1, fr);
	//fprintf(fr, "%d\n",l);
	for (i = 0;i < l; i++)
	{	//fprintf(fr, "%c %d %g",vet[i].c, vet[i].inteiro, vet[i].real);
		//fprintf(fr,"\n");
		fwrite(&vet[i].c, sizeof(char), 1, fr);
		fwrite(&vet[i].inteiro, sizeof(int), 1, fr);
		fwrite(&vet[i].real, sizeof(float), 1, fr);
	}
	fclose(fr);
	free(vet);
	texto(vet,l,fr,nome);
}
int texto(Valores *vet, int l, FILE *fr, char nome[])
{	int i;
// le vetor de arquivo binario e imprime na tela
  fr = fopen (nome, "rb");    

  if (fr == NULL) {
    perror("Erro");
    return;  
  }

  fread(&l, sizeof(int), 1, fr); // le o tamanho do vetor
  //vet = (int *)malloc(l*sizeof(int)); // aloca a estrutura
 	for (i = 0; i< l; i++)
	{
		//fscanf(fr, "%c %d %f\n",&vet[i].c, &vet[i].inteiro, &vet[i].real);//\n pra desconsiderar na leitura do arquivo
		fread(&vet[i].c, sizeof(char), 1, fr);
		fread(&vet[i].inteiro, sizeof(int), 1, fr);
		fread(&vet[i].real, sizeof(float), 1, fr);
	}	

  fclose(fr);
	printf("%d\n",l);
  for (i = 0; i < l; i++)     // imprime o vetor na tela
    printf("%c %d %g\n", vet[i].c,vet[i].inteiro,vet[i].real);

 // free (vet);
}
