/* Nome: Letícia Mara Berto
RA: 587354 */
#include <stdio.h>
#include <stdlib.h>
int main()
{
	FILE *f;
	char nome[255];
	int tam, *vet, i;
	
	scanf("%[^\n]",nome);
	
	f = fopen(nome, "rb"); 
	 
	if (f == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
 	fread(&tam, sizeof(int), 1, f); // le o tamanho do vetor
 	vet = (int *)malloc(tam*sizeof(int)); // aloca a estrutura
 	fread(vet, sizeof(int), tam, f); // le o vetor do arquivo 

 	fclose(f);

  	for (i = 0; i < tam; i++)     	
   		printf("%d\n", vet[i]);

 	free (vet);//libera a memória
	return 0;
}
