#include <stdio.h>
#include <stdlib.h>

int main() {

	FILE *write;
	int d;
	char c;
	float f;
	
	write = fopen( "v2.bin", "wb" );
	
	d = 4;
	fwrite( &d, sizeof( int ), 1, write);
	
	c = 'a';
	d = 30;
	f = 12.6;
	fwrite( &c, sizeof( char ), 1, write);
	fwrite( &d, sizeof( int ), 1, write);
	fwrite( &f, sizeof( float ), 1, write);
	
	c = 'v';
	d = 4;
	f = 8.88804;
	fwrite( &c, sizeof( char ), 1, write);
	fwrite( &d, sizeof( int ), 1, write);
	fwrite( &f, sizeof( float ), 1, write);   
	
	c = 'b';
	d = 5555;
	f = 0.0001;
	fwrite( &c, sizeof( char ), 1, write);
	fwrite( &d, sizeof( int ), 1, write);
	fwrite( &f, sizeof( float ), 1, write);

	c = 'x';
	d = 123456;
	f = 1;
	fwrite( &c, sizeof( char ), 1, write);
	fwrite( &d, sizeof( int ), 1, write);
	fwrite( &f, sizeof( float ), 1, write);

	return 0;
}
