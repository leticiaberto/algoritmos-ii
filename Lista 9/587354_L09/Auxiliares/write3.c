#include <stdio.h>
#include <stdlib.h>

int main() {

	FILE *write;
	int d;
	
	write = fopen( "vt.bin", "wb" );
	
	d = 4;
	fwrite( &d, sizeof( int ), 1, write);
	d = 5;
	fwrite( &d, sizeof( int ), 1, write);
	d = 6;
	fwrite( &d, sizeof( int ), 1, write);
	d = 7;
	fwrite( &d, sizeof( int ), 1, write);
	d = 8;
	fwrite( &d, sizeof( int ), 1, write);
	d = 9;
	fwrite( &d, sizeof( int ), 1, write);
	
	fclose(write);
	return 0;
}
