#include <stdio.h>
#include <stdlib.h>

int main() {

	FILE *read;
	int tam, i;
	int d;
	char c;
	float f;
	
	read = fopen( "vt.bin", "rb" );
	
	fread( &tam, sizeof( int ), 1, read);
	printf("%d\n", tam);
	
	for( i = 0 ; i < tam ; i++ ) {
		fread( &c, sizeof( char ), 1, read);
		fread( &d, sizeof( int ), 1, read);
		fread( &f, sizeof( float ), 1, read);
		printf("%c %d %f\n", c, d, f );
	}
	
	return 0;
}
