/* Nome: Letícia Mara Berto
RA: 587354 */
#include <stdio.h>
#include <stdlib.h>
int main()
{
	FILE *f1, *f2;
	char nome1[255], nome2[100];
	int l,tam, *v, i;
	
	scanf("%[^\n]%*c", nome1);
	scanf("%[^\n]", nome2);
	scanf("%d",&l);
	
	f1 = fopen(nome1, "rb");
	if (f1 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
		
	fread(&tam, sizeof(int), 1, f1); // le o tamanho do vetor
	//printf("tam: %d\n",tam);
	v = (int *)malloc(tam*sizeof(int)); // aloca a estrutura
  	fread(v, sizeof(int), tam, f1); // le o vetor do arquivo 
   //printf("tam: %d\n",tam);
  	
  	fclose(f1);
  	
  	f2 = fopen(nome2, "w");
	if (f2 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
  	for (i = 0; i < tam; i++)
   	{
   		if (v[i] > l)
   			fprintf(f2, "%d\n", v[i]);
   	}
   	free (v);
	fclose(f2); 
 	
	return 0;
}
