/* Nome: Letícia Mara Berto
RA: 587354 */
#include<stdio.h>
#include <stdlib.h>
typedef struct _chave
{
	int valor;
	int chave;
}Chave;

void ordenar(Chave *novo, int k);

int main()
{
	FILE *fb, *ft;
	char nome1[255], nome2[255];
	int tam, m;
	Chave *valores;
	scanf("%[^\n]%*c",nome2);
	scanf("%[^\n]%*c",nome1);	

	ft = fopen(nome2, "r");
	
	if (ft == NULL){
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	fscanf(ft,"%d", &tam);
	//printf("%d\n",tam);
	Chave *novo = (Chave *) malloc (tam*sizeof(Chave));//aloca na memoria
	for(m = 0; m < tam; m++)
	{	
		fscanf(ft,"%d@",&novo[m].chave);
		fscanf(ft,"%d#",&novo[m].valor);
		
		//printf("%d %d\n",novo[m].chave,novo[m].valor);
	}

	ordenar(novo, m);
	
	/*for(m = 0; m < tam; m++)
	{
		printf("%d %d\n",novo[m].chave,novo[m].valor);
		
	}*/
	fclose(ft);
	
	fb = fopen(nome1, "wb");
	if (fb == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	for (m = 0;m< tam; m++)
	{
		fwrite(&novo[m].chave, sizeof( int ), 1, fb);
		fwrite(&novo[m].valor, sizeof( int ), 1, fb);
	}
	fclose(fb);
	free(novo);
	return 0;
}
void ordenar(Chave *novo, int k)
{
	Chave aux;
	
	int j, i;
			
      for(i = 0; i < k-1; i++)
      {
        for(j=i+1; j < k ; j++)
	 	{
            if(novo[i].chave > novo[j].chave)
	   		{
               aux = novo[i];
               novo[i] = novo[j];
               novo[j] = aux;
            }
         }
      }  
}
