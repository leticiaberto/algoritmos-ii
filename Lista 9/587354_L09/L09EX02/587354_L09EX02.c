/* Nome: Letícia Mara Berto
RA: 587354 */
#include <stdio.h>
#define TAM_BLOCO       512

int main ()
{
	FILE *f1, *f2;
	char nome1[255], nome2[255];
	int nr, nw;
 	char bloco[TAM_BLOCO];
	scanf("%[^\n]%*c",nome1);
	scanf("%[^\n]%*c",nome2);
	
	f1 = fopen(nome1, "rb");
	if (f1 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	f2 = fopen(nome2, "wb");
	if (f2 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	while(!feof(f1))
	{
		nr = fread(&bloco, sizeof(char), TAM_BLOCO, f1);//nr armazena a quantidade de elementos lidos, quando chega no fim do arq retorna 0
   		nw = fwrite(&bloco, sizeof(char), nr, f2);
	}
	
	fclose(f1);
	fclose(f2);
	return 0;
}
