/* Nome: Letícia Mara Berto
RA: 587354 */
typedef struct _chave
{
	int chave;
	int valor;
}Chave;
#include<stdio.h>
#include <stdlib.h>
int main()
{
	FILE *fb, *ft;
	char nome1[255], nome2[255];
	int i = 0, j,aux;
	Chave *vet;
	scanf("%[^\n]%*c",nome1);
	scanf("%[^\n]",nome2);
	
	fb = fopen(nome1, "rb");
	if (nome1 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	ft = fopen(nome2, "w");
	if (nome2 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	vet = (Chave *)malloc(1*sizeof(Chave)); // aloca a estrutura
	
	while(!feof(fb))
	{
		fread(&aux, sizeof(int), 1, fb);//conta a chave e o valor como um registro só
		if (feof(fb)) break;
		i++;
	}
	i = i/2;//pq conta duas vezes para o mesmo registro
	rewind(fb);
	fprintf(ft, "%d ", i);
	i = 0;
	while(!feof(fb))
	{
		fread(&vet[i].chave, sizeof(int), 1, fb);
		fread(&vet[i].valor, sizeof(int), 1, fb);
		if (feof(fb)) break;
		fprintf(ft, "%d@", vet[i].chave);
		fprintf(ft, "%d#", vet[i].valor);
		printf("%d %d\n",vet[i].chave, vet[i].valor);
		i++;
		vet = (Chave *)malloc(i*sizeof(Chave)); // aloca a estrutura
	}
	
	fclose(fb);
	fclose(ft);
	return 0;
}
