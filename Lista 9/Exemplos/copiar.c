/*
 * C�pia de arquivo 
 */
 
#include <stdio.h>

#define TAM_BLOCO       512
#define FILE_NAME_SIZE  256

int main () {
  
  FILE *fr, *fw;
  int nr, nw;
  char bloco[TAM_BLOCO];
  char file_origem[FILE_NAME_SIZE], file_destino[FILE_NAME_SIZE];
  
  scanf("%255[^\n]%*c", file_origem);
  scanf("%255[^\n]%*c", file_destino);  

  fr = fopen (file_origem, "rb");

  if (fr == NULL) {
    perror("Erro na abertura do arquivo de origem");
    return 2;
  }

  fw = fopen (file_destino, "wb");

  if (fw == NULL) {
    perror("Erro na abertura do arquivo de destino");
    return 3;
  }
  
  do {
    nr = fread(&bloco, sizeof(char), TAM_BLOCO, fr);
    nw = fwrite(&bloco, sizeof(char), nr, fw);
  } while (!feof(fr));

  fclose(fr);  
  fclose(fw);
  
  return (0);

}
