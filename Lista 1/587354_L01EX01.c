/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 2
	Prof. Tiago A. Almeida

	Lista 01 - Exercício 01 - Imprime numero em ordem inversa

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */


#include <stdio.h>

int main()
{
	long long int num;//tem que por um long a mais por causa dos bits (diferença entre 32 e 64 bits)
	scanf ("%lld",&num);
	do
	{
		printf ("%lld",num % 10);//o resto da divisaõ por 10 sempre resultará no ultimo algarismo
		num = num/10;//para diminuir o numero e eliminar o ultimo algarismo
	}while(num>0);
}
