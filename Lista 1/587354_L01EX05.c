/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 5
	Prof. Tiago A. Almeida

	Lista 01 - Exercício 05 - Quadrado mágico

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */


#include <stdio.h>

#define MAX		100
#define TRUE	1
#define FALSE	0	
#define FRASE_MAGICO 		"MAGICO"
#define FRASE_NAO_MAGICO 	"NAO"

typedef int boolean;


int main () {

	int n,i,j,quad[MAX][MAX],flag=1,soma1=0,soma2=0,k,num,vet[MAX],c=0;

	scanf ("%d",&n);//recebe a ordem do quadrado

	//recebe os valores do quadrado
	for (i=0;i<n;i++)
	{
		for (j=0;j<n;j++)
		{
			scanf ("%d",&quad[i][j]);
		}
	}
	//verifica condições dos valores, estar entre 1 e n ao quadrado
	for (i=0;i<n;i++)
	{
		for (j=0;j<n;j++)
		{
			if(quad[i][j] > (n*n) || quad[i][j] < 1)
			{
				printf (FRASE_NAO_MAGICO);
				return 0;
			}	
		}
	}
	//Passa os valores da matriz para um vetor
	for (i=0;i<n;i++)
	{
		for (j=0;j<n;j++)
		{	vet[c] = quad[i][j];
			c++;
		}
	}
	/*for (c=0;c<n*n;c++)
	{printf ("%d",vet[c]);}*/
	//verifica se os elementos se repetem, se repetirem nao é mágico
	for (i=0;i<c;i++)
	{
		num = vet[i];
		j=i+1;
		for (j;j<=c;j++)
		{
			if (num == vet[j])
			{	printf (FRASE_NAO_MAGICO);
				return 0;
			}
		}
	}

		//atribue o valor a variavel soma com os valores da primeira linha
		for (j=0;j<n;j++)
		{
			soma1 = soma1 + quad[0][j];
		}
	//verifica a soma das linhas
	for (i=0;i<n;i++)
	{	
		soma2=0;
		for (j=0;j<n;j++)
		{
			soma2 = soma2 + quad[i][j];
		}
			if (soma2 == soma1)
				flag = 1;
			else
				flag = 0;
			if (flag == 0)//ja descarta a possibilidade de ser magico
			{	printf (FRASE_NAO_MAGICO);
				return 0;
			}
	}

	//verifica a soma das colunas
	for(j=0; j<n; j++)
	{	soma2 = 0;
		for(i=0; i<n; i++)
		{	
				soma2 = soma2 + quad[i][j];
		}
		if (soma2 == soma1)
				flag = 1;
			else
				flag = 0;
			if (flag == 0)//ja descarta a possibilidade de ser magico
			{	printf (FRASE_NAO_MAGICO);
				return 0;
			}
	}
	


	//verifica a soma da diagonal principal
	soma2 = 0;
	for (i=0;i<n;i++)
	{		
		for (j=0;j<n;j++)
		{
			if (j==i)
				soma2 = soma2 + quad[i][j];
		}
		if (soma2 == soma1)//verifica  fora do for pq senao dará errado
		flag = 1;
	else
		flag = 0;			
	}
	//printf ("%dsoma dp: ",soma2);
	if (flag == 0)//ja descarta a possibilidade de ser magico
	{	printf (FRASE_NAO_MAGICO);
		return 0;
	}

	//verifica a soma da diagonal secundaria
	soma2 = 0;
	k=0;
	for (i=1;i<=n;i++)
	{
		for (j=1;j<=n;j++)
		{
			if (j==n-k)
				soma2 = soma2 + quad[i-1][j-1];//printf("%d\n",soma2);
		}
		k++;
	}
	if (soma2 == soma1)//verifica  fora do for pq senao dará errado
		flag = 1;
	else
		flag = 0;
	if (flag == 0)//ja descarta a possibilidade de ser magico
	{	printf (FRASE_NAO_MAGICO);
		return 0;
	}

	
	/*for (i=1;i<n*n;i++)
	{
		if (valor == vet[i])
		{
			printf (FRASE_NAO_MAGICO);
			return 0;
		}
	}*/	
	
	//se for magico
	printf (FRASE_MAGICO);
	return 0;
}
