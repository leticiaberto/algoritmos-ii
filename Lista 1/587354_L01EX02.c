/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 2
	Prof. Tiago A. Almeida

	Lista 01 - Exercício 02 - Palíndromos entre m e n

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letíca Mara Berto

* ================================================================== */

#include <stdio.h>

int main()
{
	long int n,m,i,digito,invertido = 0,num;
	//invertido armazenará o numero no processo de inversão
	//num armazenará o numero do intervalo para que nao o perca e seja possivel comparar no final

	//consistencia de dados
	do
	{
		scanf ("%ld",&m);
		scanf ("%ld",&n);
	}while (n<m || n>1000000 || n==m);
	
	for (i=m;i<=n;i++)//percorre todos os valores do intervalo informado
	{	num = i;
		//transforma o numero de tras pra frente
		do
		{
			digito = num % 10;//o resto da divisaõ por 10 sempre resultará no ultimo algarismo
			num = num/10;//para diminuir o numero e eliminar o ultimo algarismo
			invertido = (invertido * 10) + digito;
		}while(num>0);

		if (invertido == i)
			printf ("%ld ",invertido);
		invertido = 0;// é necessario reiniciar a variavel para o novo calculo
	}
	return 0;//para nao dar erro no caso de nao ter nenhum palindromo
}
