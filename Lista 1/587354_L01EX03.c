/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 2
	Prof. Tiago A. Almeida

	Lista 01 - Exercício 03 - Conjectura de Goldbach

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

/* <<< ESCREVA SEU CODIGO AQUI >>> */
#include <stdio.h>
#define MAX 1000

int soma (int primos[], int i, int c, int g, int usados[]);

int main()
{
	int n,m,i,cont,dive,flag,j,aux,p1,p2,c,primos[MAX],g=1,usados[MAX];//p1 = primo1, c = contador do vetor com os numeros primos

	do
	{
		scanf("%d",&m);
		scanf("%d",&n);
	}while(n<m || m<2 || n>1000 || n==m);//verificação das entradas do sistema. Deve serguir os requisitos: n > m, m ≥ 2 e n ≤ 1.000

	//inicializa as variaveis	
 	cont = 2;
	dive = 2;
	j = 2;
	flag=1;
	c = 0;
	//percorre o intervalo e encontra os primos dentro do intervalo informado
	for (i=m;i<=n;i++)//percorre o intervalo dado
	{	
		//verifica se o numero é par. Se for,ele verifica a soma dos primos menores que ele que o compõe	
		if(i % 2 == 0)
		{
			//primos menores que o numero em questão
			while (cont<i)
			{
				 if (i==2)//não tem primo menor que 2
				 {
					break;
				 }

				j = 2;
		
				while (j < dive) 
				{
				    aux = dive % j;
				    //printf ("%d",aux);
				    if ( aux == 0)
				    {
					flag = 0;
					//printf ("entrou");
				    }  
					
				    j = j + 1;
				
				}//while
				primos[c] = j;
				//printf ("%d ",primos[c]);
				if (flag==1) 
				    {
					//flag = 1;
					//printf ("%d ",primos[c]);
					c++;
					//soma (primos,i,c);
				    }   
		
				cont = cont + 1;
				dive = dive+1;
				flag = 1;

		    	}//while
		soma (primos,i,c,g,usados);
		}//if
	}//for
	
	
	return (0);	
}

int soma (int primos[], int i, int c, int g, int usados[])
{
	int j,k,h,flag=0;
	for (k=0;k<c;k++)
	{
		for(j=k;j<c;j++)
		{
			if (primos[k] + primos[j] == i)//a soma dos numeros resulta no numero par em questão(i)
			{	 for (h=0;h<g;h++)
				 {	
					if (usados[h] != i)
				 		flag = 1;//a soma do numero par ainda nao foi encontrada
					else
						flag = 0;
				 }
				if (flag == 1)
				{	printf ("%d = %d + %d\n",i,primos[k],primos[j]);
					g++;
					usados[h]=i;//para nao pegar todos os numeros possiveis dentro do intervalo que somados dao o valor desejado, ou seja, ao encontrar a primeira combinação(a menor,consequentemente) já exluimos a possiblidade de outros numeros com a verificação feita acima -> usados[h] != i 
				 	break;
				}
			}
		}
	}
}
//Obs: o numero 2 é par mas é a soma de dois primos
