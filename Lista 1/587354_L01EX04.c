/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 01 - Exercício 01 - Lista Estática

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

#define MAX 100 // tamanho maximo da lista

// menu de opcoes
#define INICIALIZAR 		1
#define ADICIONAR 			2
#define ADICIONAR_POSICAO 	3
#define REMOVER 			4
#define REMOVER_OCORRENCIAS 5
#define BUSCAR 				6
#define IMPRIMIR			7
#define IMPRIMIR_POSICAO	8	
#define SAIR 				0

// frases pre-definidas para saida
#define FRASE_LISTA_CHEIA "Lista cheia!\n"
#define FRASE_LISTA_VAZIA "Lista vazia!\n"
#define FRASE_ELEM_NAO_ENCONTRADO "Elemento nao encontrado!\n"
#define FRASE_POSICAO_NAO_EXISTENTE "Posicao nao existente!\n"
#define FRASE_POSICAO_VAZIA "Posicao vazia!\n"


//Declaracoes das funcoes
/* <<< DECLARE AS FUNCOES AQUI!!! >>> */
void inicializarLista(int Lista[], int qt_elementos);
void adicionarElemento(int Lista[], int qt_elementos, int valor);
void adicionarPosicao(int Lista[], int qt_elementos, int valor, int posicao);
void removerPosicao(int Lista[], int qt_elementos, int posicao);
int removerOcorrencias(int Lista[], int qt_elementos, int valor);
int buscarValor(int Lista[], int qt_elementos, int valor);
void imprimirLista(int Lista[], int qt_elementos);
void imprimirPosicao(int Lista[], int posicao);
/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main()
{
	int Lista[MAX],		// lista
	    qt_elementos=0,	// quantidade de elementos na lista
	    opcao,			// opcao selecionada pelo usuario
	    valor,			// valor informado pelo usuario
	    posicao,		// posicao informada pelo usuario
	    qt_removidos;	// armazena a quantidade de elementos removidos na opcao 5
	   

	scanf("%d", &opcao);

	while (opcao != SAIR)
	{
		switch(opcao)
		{
			case INICIALIZAR:
				if (qt_elementos == 0) {
					scanf("%d", &qt_elementos);
					inicializarLista(Lista, qt_elementos);
				}
				else
					printf("A lista contem elementos!\n");
				
			break;

			case ADICIONAR:
				if (qt_elementos == MAX) printf(FRASE_LISTA_CHEIA);
				else {
					scanf("%d", &valor);
					adicionarElemento(Lista, qt_elementos, valor);
					qt_elementos++;
				}

			break;		

			case ADICIONAR_POSICAO:
				if (qt_elementos == MAX) printf(FRASE_LISTA_CHEIA);
				else {
					scanf("%d", &posicao);
				
					if (posicao > qt_elementos) printf(FRASE_POSICAO_NAO_EXISTENTE);
					else {
						scanf("%d", &valor);
						adicionarPosicao(Lista, qt_elementos, valor, posicao);
						qt_elementos++;
					}
				}

			break;

			case REMOVER:

				if (qt_elementos == 0) printf(FRASE_LISTA_VAZIA);
				else {
					scanf("%d", &posicao);

					if ((posicao > qt_elementos) || (posicao < 1)) printf(FRASE_POSICAO_NAO_EXISTENTE);
					else {
						removerPosicao(Lista, qt_elementos, posicao);
						qt_elementos--;
					}
				}

			break;


			case REMOVER_OCORRENCIAS:

				if (qt_elementos == 0) printf(FRASE_LISTA_VAZIA);
				else {
					scanf("%d", &valor);
					qt_removidos = removerOcorrencias(Lista, qt_elementos, valor);
					if (qt_removidos == 0) printf(FRASE_ELEM_NAO_ENCONTRADO);
					else qt_elementos = qt_elementos - qt_removidos;

				}
			break;


			case BUSCAR:
				if (qt_elementos == 0) printf(FRASE_LISTA_VAZIA);
				else {
					scanf("%d", &valor);
					if (buscarValor(Lista, qt_elementos, valor) == 0) printf(FRASE_ELEM_NAO_ENCONTRADO);
				}

			break;

			case IMPRIMIR:
				if (qt_elementos == 0) printf(FRASE_LISTA_VAZIA);
				else {
					imprimirLista(Lista, qt_elementos);
				}

			break;

			case IMPRIMIR_POSICAO:
				if (qt_elementos == 0) printf(FRASE_LISTA_VAZIA);
				else {
					scanf("%d", &posicao);

					if (posicao > MAX) printf(FRASE_POSICAO_NAO_EXISTENTE);
					else if (posicao > qt_elementos) printf(FRASE_POSICAO_VAZIA);
					else imprimirPosicao(Lista, posicao);
				}

			break;			
		}

		scanf("%d", &opcao);
	}

	return 0;
}


/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */

void imprimirLista(int Lista[], int qt_elementos){
	int i;

	for (i = 0; i < qt_elementos; i++){
		printf("%d ", Lista[i]);
	}

	printf("\n");
}


void imprimirPosicao(int Lista[], int posicao){
	printf("%d\n", Lista[posicao-1]);
}


/* <<< IMPLEMENTE AS DEMAIS FUNCOES AQUI!!! >>> */
void inicializarLista(int Lista[], int qt_elementos)
{
	int i;
	for (i=0;i<qt_elementos;i++)
	{
		scanf ("%d",&Lista[i]);
	}
}
void adicionarElemento(int Lista[], int qt_elementos, int valor)
{
	int i;
	i = qt_elementos;//primeira posição vazia
	Lista[i] = valor;
}
void adicionarPosicao(int Lista[], int qt_elementos, int valor, int posicao)
{
	int i,j,elementos,flag = 0,
	    listanova[MAX+1];//acrescentará um elemento,entao o tamanho é um a mais que o máximo

	elementos = qt_elementos + 1;//ao acrescentar um novo elemento, a quantidade aumenta em 1

	for (i=0;i < elementos;i++)
	{
		if ((i + 1) == posicao)//a posição no vetor começa no zero, mas o usuario digita a partir do um, logo é necessario colocar i+1 para manipular as posições de acordo com a contagem do usuario
		{
			for (j=i+1;j <= elementos; j++)//deslocamos os numeros para a direita com o comendo j=i+1
			{
				listanova[j] = Lista[j-1];//o j-1 acompanha o i da Lista para pegar os valores que tinha antes de adicionar a posição
			}
			listanova[i] = valor;
			flag = 1;//indica que a posição ja foi encontrada e que o novo vetor a ser apresentado ja esta ok
			//printf ("%d",listanova[i]);
		}
		else
			if (flag==0)//flag=0 indica que a posição ainda nao foi encontrada, logo populamos o novo vetor de acordo com a ordem do Lista[]
				listanova[i] = Lista[i];
	}
	for (i=0;i< elementos;i++)//joga os valores do novo vetor para o vetor Lista[] para poder fazer a impressao correta
	{
		Lista[i] = listanova[i];
	}
	
}
void removerPosicao(int Lista[], int qt_elementos, int posicao)
{
	int i,j,elementos,flag = 0,
	    listanova[MAX-1];//excluirá um elemento,entao o tamanho é um a menos que o máximo

	elementos = qt_elementos - 1;//ao remover um novo elemento, a quantidade diminui em 1

	for (i=0;i < elementos;i++)
	{
		if ((i + 1) == posicao)//a posição no vetor começa no zero, mas o usuario digita a partir do um, logo é necessario colocar i+1 para manipular as posições de acordo com a contagem do usuario
		{
			for (j=i;j <= elementos; j++)//deslocamos os numeros para a esquerda com o comendo j=i
			{
				listanova[j] = Lista[j+1];//o j+1 acompanha as psoições apos a desejada para exluir, e para pegar os valores que tinha antes de adicionar a posição
			}
			//listanova[i] = valor;
			flag = 1;//indica que a posição ja foi encontrada e que o novo vetor a ser apresentado ja esta ok
			//printf ("%d",listanova[i]);
		}
		else
			if (flag==0)//flag=0 indica que a posição ainda nao foi encontrada, logo populamos o novo vetor de acordo com a ordem do Lista[]
				listanova[i] = Lista[i];
	}
	for (i=0;i< elementos;i++)//joga os valores do novo vetor para o vetor Lista[] para poder fazer a impressao correta
	{
		Lista[i] = listanova[i];
	}
	
}
int removerOcorrencias(int Lista[], int qt_elementos, int valor)
{
	int i,j=0,listanova[MAX],removidos=0;

	for(i=0;i < qt_elementos; i++)
	{
		if (Lista[i] != valor)//se for diferente do valor que deseja exluir inclui no novo vetor
		{		
			listanova[j] = Lista[i];
			j++;
		}
		else
			removidos = removidos + 1;
	}
	for (i=0;i< j;i++)//joga os valores do novo vetor para o vetor Lista[] para poder fazer a impressao correta
	{
		Lista[i] = listanova[i];
	}
	return (removidos);//retorna a quantidade de elementos removidos
}
int buscarValor(int Lista[], int qt_elementos, int valor)
{
	int i,posicao[MAX],j=0;

	for (i=0;i < qt_elementos; i++)
	{
		if (Lista[i] == valor)
		{
			posicao[j] = i+1;//a posição no vetor começa no zero, mas o usuario digita a partir do um, logo é necessario colocar i+1 para manipular as posições de acordo com a contagem do usuario
			j++;
		}
	}
	if (j != 0)//indica que encontrou o numero
	{
		//imprimir posições
		for (i = 0; i < j; i++)
		{
			printf("%d ", posicao[i]);
		}
		printf("\n");
	}
	if (j == 0)
		return (j);
}
