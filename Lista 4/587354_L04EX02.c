/* ================================================================== *
   Universidade Federal de Sao Carlos - UFSCar, Sorocaba

   Disciplina: Algoritmos e Programação 2
   Prof. Tiago A. Almeida

   Lista 04 - Exercício 02 - Progressão Geométrica

   Instrucoes
   ----------

   Este arquivo contem o codigo que auxiliara no desenvolvimento do
   exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
   Dados do aluno:

   RA: 587354
   Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

// variavel global: mantem a quantidade de termos da progressao
int num_termos = 0;


// declaracoes das funcoes
/* <<< DECLARE O PROTOTIPO DA FUNCAO AQUI >>> */
float  progressaoGeometrica(float termoInicial,float razao,int termoN1,int termoN2, float *resultado1, float *resultado2);

/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main()
{

   // variaveis utilizadas para o calculo da progressao geometrica
   float termoInicial, razao, resultado1, resultado2 = 0;
   int termoN1, termoN2;
          
   scanf("%f %f", &termoInicial, &razao);
   scanf("%d %d", &termoN1, &termoN2);
   
   progressaoGeometrica(termoInicial,razao,termoN1,termoN2,&resultado1,&resultado2);

   printf("%.2f %.2f", resultado1, resultado2);

   return 0;
}


/* ------------------------- */
/* IMPLEMENTACAO DA FUNCAO   */
/* ------------------------- */
float  progressaoGeometrica(float termoInicial,float razao,int termoN1,int termoN2, float *resultado1, float *resultado2)
{
	*resultado1 = termoInicial;
	
	num_termos++;
	while(num_termos < termoN1)
	{
		*resultado1 = *resultado1 * razao;
		num_termos++;
	}
	*resultado2 = * resultado1;
	while(num_termos < termoN2)
	{
		*resultado2 = *resultado2*razao;
		num_termos++;
	}
	
}
