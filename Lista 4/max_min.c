#include <stdio.h>

#define TAM 10

void max_min(int const vetor[], int tam, int *max, int *min);

int main()
{
	int vetor[TAM], i, max, min;

	for (i=0;i<TAM;i++)
		scanf ("%d",&vetor[i]);

	max_min(vetor, TAM, &max, &min);

	printf ("Máximo: %d\n",max);
	printf ("Minimo: %d\n",min);

	return (0);
}

void max_min(int const vetor[], int tam, int *max, int *min)
{
	int i;
	*min = *max = vetor[0];

	for (i=1;i<TAM;i++)
	{
		if (vetor[i] > *max)
			*max = vetor[i];
		else if (vetor[i] < *min)
			*min = vetor[i];
	}
}
