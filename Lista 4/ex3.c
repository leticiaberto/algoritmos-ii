#include <stdio.h>

int *checarDesempenho(float, float, int *, int *);

int main(void)
{
	float media, porcPresenca;
	int qtAlunos, qtAprovados=0, qrReprovados=0, *situação;

	scanf("%d", &qtAlunos);

	while(qtAlunos>0)
	{
		scanf("%f %f", &media, &porcPresenca);

		situacao = checarDesempenho(media, porcPresenca, &qtAprovados, &qtReprovados);

		(*situacao)++;

		qtAlunos--;
	}

	printf("Aprovados: %d\n", qtAprovados);
	printf("Reprovados: %d\n", qtReprovados);

	return 0;
}

int *checarDesempenho(float media, float porcPresenca, int *qtAprovados, int *qtReprovados)
{
	if( (media>=6.0) && (porcPresenca>=75.0) )
		return (qtAprovados);
	else
		return (qtReprovados);
	
}

/*
http://www.ime.usp.br/~pf/algoritmos/aulas/pont.html
http://www.inf.ufpr.br/cursos/ci067/Docs/NotasAula/notas-28_Ponteiros.html
http://juliobattisti.com.br/tutoriais/katiaduarte/cbasico007.asp
*/
