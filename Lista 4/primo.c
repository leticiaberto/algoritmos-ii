#include <stdio.h>

int primo (int);
void intervalo (int, int, int *, int *);

int main()
{

	int a, b, maiorprimo = -1, pontocentral;

	scanf ("%d %d", &a, &b);

	intervalo(a, b, &maiorprimo, &pontocentral);

	if (maiorprimo > -1) {
		printf ("Maior primo: %d\n", maiorprimo);
	} else {
		printf ("Nao tem primo\n");
	}

	printf ("Ponto central: %d\n", pontocentral); 

return (0);
}

int primo (int n){
	int i, flag = 1;

	for (i = 2; i < n; i++) {
		if (n % i == 0) {
			flag = 0;
		}
	}
	return flag;
}

void intervalo (int a, int b, int *maiorprimo, int *pontocentral){

	int i;

	*pontocentral = (a + b) / 2;

	for (i = a; i <= b; i++){
		if (primo(i)) {
			if (i > *maiorprimo) {
				*maiorprimo = i;
			}
		}
	}
}
