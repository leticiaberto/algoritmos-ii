/* ================================================================== *
   Universidade Federal de Sao Carlos - UFSCar, Sorocaba

   Disciplina: Algoritmos e Programação 2
   Prof. Tiago A. Almeida

   Lista 4 - Exercício 4 - Canivete Suico para Vetores

   Instrucoes
   ----------

   Este arquivo contem o codigo que auxiliara no desenvolvimento do
   exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
   Dados do aluno:

   RA: 587354
   Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <math.h>

#define MAX 100 //Tamanho máximo do vetor.

//Enumeração das opções do menu.
enum opcoes{
         OPC_SAIR,
         OPC_IMPRIMIR,
         OPC_MAXMIN,
         OPC_SOMA,
         OPC_ESTATISTICA,
         OPC_BUSCARVALOR,
         OPC_CONTAGEM
};


// Declaração das funções.
int tamanhoVetor();
void popularVetor(int vet[], int tamanho);
void imprimirVetor(int vet[], int tamanho);
/* <<< DECLARE OS PROTOTIPOS DAS FUNCOES AQUI >>> */
int fibonacci (int vet[], int indice);
int primo (int vet[], int indice);
float ordenarCrescente(int vetor[], int numElementos, int vetorordenado[]);
int calcularMaxMin(int const vet[], int numElementos, int *maximo, int *minimo);
int calcularSomas(int vet[], int numElementos, int *somaPares, int *somaImpares, int *somaPositivos, int *somaNegativos);
float calcularEstatistica(int vet[], int numElementos, float *media, float *mediaHarmonica, float *mediana, float *desvioPadrao);
int  buscarValor(int vet[], int numElementos, int numero, int *totalOcorrencia, int *primeiraOcorrencia, int *ultimaOcorrencia);
int contarEspecial(int vet[], int numElementos, int *qtPares, int *qtImpares, int *qtPrimos, int *qtFibonacci);
/* ---------------- */
/* ROTINA PRINCIPAL */
/* ---------------- */
int main(){

   int numElementos = 0,   //Armazena o tamanho do vetor.
       opcao,              //Armazena o a opção selecionada pelo usuário.
       vetor[MAX],
       numero;             //Armazena o número digitado pelo usuário para contagem e busca.

   // variaveis para a rotina de maximo e minimo
   int maximo, minimo;

   // variaveis para a rotina de soma
   int somaPares, somaImpares, somaPositivos, somaNegativos;

   // variaveis para a rotina de estatistica
   float media, mediaHarmonica, mediana, desvioPadrao;

   // variaveis para a rotina de contagem
   int qtPares, qtImpares, qtPrimos, qtFibonacci;

   // variaveis para a rotina de busca por valor
   int totalOcorrencia, primeiraOcorrencia, ultimaOcorrencia;

   numElementos = tamanhoVetor();
   popularVetor(vetor, numElementos);

   scanf("%d", &opcao);     //Lê a opção selecionada pelo usuário.

   //Executa as opções enquanto o usuário não digitar a opção 0 - Sair.
   while(opcao != OPC_SAIR){

      switch(opcao){         

         //----------------------------------------------------------
            case OPC_IMPRIMIR:
               imprimirVetor(vetor, numElementos);
            break;
         //----------------------------------------------------------

         //----------------------------------------------------------
            case OPC_MAXMIN:
               calcularMaxMin(vetor, numElementos, &maximo, &minimo);
               printf("%d %d\n", maximo, minimo);
            break;
         //----------------------------------------------------------

         //----------------------------------------------------------
            case OPC_SOMA:
               calcularSomas(vetor, numElementos, &somaPares, &somaImpares, &somaPositivos, &somaNegativos);
               printf("%d %d %d %d\n", somaPares, somaImpares, somaPositivos, somaNegativos);
            break;
         //----------------------------------------------------------

         //----------------------------------------------------------
            case OPC_ESTATISTICA:
               // lembre-se de considerar apenas numeros maiores que zero na media harmonica
               // para o desvio padrao, considere o vetor como amostra e nao a populacao toda
               calcularEstatistica(vetor, numElementos, &media, &mediaHarmonica, &mediana, &desvioPadrao);
               printf("%.2f %.2f %.2f %.2f\n", media, mediaHarmonica, mediana, desvioPadrao);
            break;
         //----------------------------------------------------------

         //----------------------------------------------------------
            case OPC_BUSCARVALOR:
               scanf("%d",&numero); //le qual numero esta sendo buscado
               buscarValor(vetor, numElementos, numero, &totalOcorrencia, &primeiraOcorrencia, &ultimaOcorrencia);
               printf("%d %d %d\n", totalOcorrencia, primeiraOcorrencia, ultimaOcorrencia);
            break;
         //----------------------------------------------------------

         //----------------------------------------------------------
            case OPC_CONTAGEM:
               contarEspecial(vetor, numElementos, &qtPares, &qtImpares, &qtPrimos, &qtFibonacci);
               printf("%d %d %d %d\n", qtPares, qtImpares, qtPrimos, qtFibonacci);
            break;
         //----------------------------------------------------------

      }

      scanf("%d", &opcao);      
   }

   return 0;

}

/* --------------------------------------- */
/* IMPLEMENTACAO DAS FUNCOES - NAO ALTERAR */
/* --------------------------------------- */

//Lê o tamanho do vetor passado pelo usuário e retorna o valor.
int tamanhoVetor(){
   int tamanho;

   do{
      scanf("%d", &tamanho);
   }while(tamanho < 1 || tamanho > 100);

   return tamanho;
}

//Lê os elementos do vetor passados pelo usuário.
void popularVetor(int vet[], int tamanho){
   int i;

   for(i = 0; i < tamanho; i++)
      scanf("%d", &vet[i]);
}

//Imprime os elementos do vetor.
void imprimirVetor(int vet[], int tamanho){
   int i;

   for(i = 0; i < tamanho; i++)
      printf("%d ", vet[i]);

   printf("\n");
}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */
int calcularMaxMin(int const vet[], int numElementos, int *maximo, int *minimo)
{
	int i;
	
	*minimo = *maximo = vet[0];

	for(i=1;i<numElementos;i++)
	{
		if (vet[i] > *maximo)
			*maximo = vet[i];
		else if(vet[i] < *minimo)
			*minimo = vet[i];
	}
}
int calcularSomas(int vet[], int numElementos, int *somaPares, int *somaImpares, int *somaPositivos, int *somaNegativos)
{
	int i,zero=0;
	*somaImpares = zero; *somaPares = zero; *somaPositivos = zero; *somaNegativos = zero;//para nao incializar a variavekde forma direta, pois é perigoso (slides de aula)
	for(i=0;i<numElementos;i++)
	{
		//verifica se é par ou impar
		if (vet[i] % 2 == 0)
			*somaPares = *somaPares + vet[i];
		else
			*somaImpares = *somaImpares + vet[i];
		//verifica se é positivo ou negativo
		if (vet[i] > 0)
			*somaPositivos = *somaPositivos + vet[i];
		else
			*somaNegativos = *somaNegativos + vet[i];
	}
}
float calcularEstatistica(int vet[], int numElementos, float *media, float *mediaHarmonica, float *mediana, float *desvioPadrao)
{
	int i,vetorordenado[MAX];
	float m1, m2, mediageral = 0, mh = 0, aux = 0, zero = 0, positivos = 0, um = 1;
	*media = zero; *mediaHarmonica = zero; *mediana = zero; *desvioPadrao = zero;

	//media e media harmonica
	for(i=0;i<numElementos;i++)
	{
		//media
		*media = *media + vet[i];
		//media harmonica
		if (vet[i] > 0)//apenas numeros positivos 
		{	mh = mh + (um/vet[i]);
			positivos++;
		}
	}
	*media = *media/numElementos;
	*mediaHarmonica = positivos/ mh;
	//desvio padrão
	for (i=0;i<numElementos;i++)
	{
		aux = pow((vet[i]-*media),2) + aux;
	}
	*desvioPadrao = sqrt((aux/(numElementos-1)));

	//mediana
	ordenarCrescente (vet, numElementos, vetorordenado); // Ordenando conjunto numerico.
	switch (numElementos % 2) 
	{
		case 0: // Faixa de valores (qtd de elem do vetor) e PAR.
			m1 = vetorordenado[numElementos / 2 - 1];
			m2 = vetorordenado[numElementos / 2];    
			m1 = (m1 + m2)/2;
			break;
		case 1: // Faixa de valores do vetor e IMPAR.
			m1 = vetorordenado[ (numElementos - 1) / 2 ];
			break;
	}  
	*mediana = m1;
	
	//*mediana = m1;
	//*desvioPadrao
}
float ordenarCrescente(int vetor[], int numElementos, int vetorordenado[])//Metodo Bolha
{
      float aux;
      int j, i;
	for (i=0;i<numElementos;i++)	
		vetorordenado[i] = vetor[i];
       
      for(i = 0; i < numElementos-1; i++)
      {
         for(j=i+1; j < numElementos ; j++)
	 {
            if(vetorordenado[i] > vetorordenado[j])
	    {
               aux = vetorordenado[i];
               vetorordenado[i] = vetorordenado[j];
               vetorordenado[j] = aux;
            }
         }
      }  
}
int  buscarValor(int vet[], int numElementos, int numero, int *totalOcorrencia, int *primeiraOcorrencia, int *ultimaOcorrencia)
{
	int i, naoEncontrou = -1, zero = 0;
	*primeiraOcorrencia = naoEncontrou;
	*ultimaOcorrencia = naoEncontrou;
	*totalOcorrencia = zero;

	for(i=0;i<numElementos;i++)
	{
		if (vet[i] == numero && *primeiraOcorrencia == naoEncontrou)
		{
			*primeiraOcorrencia = i;
			*ultimaOcorrencia = i;//caso só exista uma vez o valor desejado
			(*totalOcorrencia)++;
		}
		else if (vet[i] == numero && *primeiraOcorrencia != naoEncontrou)
			{	
				*ultimaOcorrencia = i;
				(*totalOcorrencia)++;
			}
	}
}
int contarEspecial(int vet[], int numElementos, int *qtPares, int *qtImpares, int *qtPrimos, int *qtFibonacci)
{
	int i,zero = 0;
	*qtPares = zero;
	*qtImpares = zero;
	*qtPrimos = zero;
	*qtFibonacci = zero;

	for(i=0;i<numElementos;i++)
	{
		//verifica pares e impares
		if (vet[i] % 2 == 0)
			(*qtPares)++;
		else
			(*qtImpares)++;
		//verifica se é primo
		if (primo(vet,i))
			(*qtPrimos)++;
		//verifica se é de fibonacci
			if(fibonacci(vet,i))
				(*qtFibonacci)++;
	}
}
int primo (int vet[], int indice){
	int i,flag = 1;
	if (vet[indice] < 2 && vet[indice] > 0)
		flag = 0;
	else
	{	if (vet[indice] == 1 || vet[indice] == -1)
			flag = 0;
	else
	{
		if (vet[indice] < 0)
			vet[indice] = -1*vet[indice];
			
	
		for (i = 2; i < vet[indice]; i++) 
		{
			if (vet[indice] % i == 0) {
				flag = 0;
			}
		}
	}
	}
	return flag;
}

int fibonacci (int vet[], int indice)
{
	int soma = 0, aux2 = 0, aux1 = 1, cont = 0;

	while (soma<vet[indice])
	{
		aux2=aux1;
		aux1=soma;
		soma=aux1 + aux2;
		if (soma == vet[indice])
			cont++;
	}//while
	return (cont);
}
