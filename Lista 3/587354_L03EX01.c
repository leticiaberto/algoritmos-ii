/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 2
	Prof. Tiago A. Almeida

	Lista 03 - Exercício 01 - Coeficiente Binomial

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Leticia Mara Berto

* ================================================================== */

/* <<< ESCREVA SEU CODIGO AQUI >>> */
#include <stdio.h>
double fatorial(int num);
int main()
{
	int n,k;
	double kfat,nfat,nkfat;
	//do{
		scanf ("%d %d",&n,&k);
	//}while(k<0 || n<k || n>100);

	nfat = fatorial(n);
	kfat = fatorial(k);
	nkfat = fatorial(n-k);
	printf("%.0lf\n",nfat/(kfat*nkfat));
	return (0);
}

double fatorial(int num)
{
	if (num==0)//caso base
		return 1;
	else
		return num*fatorial(num-1);
}
