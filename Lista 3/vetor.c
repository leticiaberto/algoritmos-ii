#include <stdio.h>

#define MAX 100

int soma(int vetor[], int tam);
int maior(int vetor[], int tam);

int main()
{
	int tam,vetor[MAX],i,n;	

	scanf("%d",&tam);

	for (i=0; i<tam; i++)
	{
		scanf("%d",&vetor[i]);
	}

	printf("Soma: %d\n",soma(vetor,tam-1));
	printf("Maior: %d\n",maior(vetor,tam-1));//tam = 1, posição 0 (igualar a qntd de elementos com a posição do vetor)

	return (0);
}
int soma(int vetor[], int tam)
{
	if (tam==0)//caso base - apenas um elemento no vetor
		return (vetor[tam]);
	else
		 return (vetor[tam] + soma(vetor,tam-1));
}
int maior(int vetor[], int tam)
{
	int maiorvalor;
	if(tam==0)
		return vetor[tam];
	else
		maiorvalor = maior(vetor,tam-1);
	if (vetor[tam] > maiorvalor)
		return (vetor[tam]);
	else
		return (maiorvalor);
}

