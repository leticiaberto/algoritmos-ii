/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 2
	Prof. Tiago A. Almeida

	Lista 03 - Exercício 02 - Conjectura de Collatz

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

/* <<< ESCREVA SEU CODIGO AQUI >>> */
#include <stdio.h>
int collatz(int n);
int main()
{
	int n,result;
	scanf ("%d",&n);
	printf("%d ",n);//ĩmprimir o primeiro termo
	collatz(n);
	return (0);
}

int collatz(int n)
{
	if (n==2)//caso base
	{
		printf ("%d ",1);
		return 1;
	}
	else
		if (n%2 == 0)
		{
			printf ("%d ",n/2);
			return(collatz(n/2));
		}
		else
		{
			printf ("%d ",3*n+1);
			return(collatz(3*n+1));
		}
}
