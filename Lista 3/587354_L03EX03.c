/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 2
	Prof. Tiago A. Almeida

	Lista 03 - Exercício 03 - Conversao Binaria

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

/* <<< ESCREVA SEU CODIGO AQUI >>> */
#include <stdio.h>
int binario(int n);

int main()
{
	int n;

	scanf ("%d",&n);

	binario(n);
	printf("\n");
	return (0);
}

int binario(int n)
{
	if (n==1)//caso base
	{
		printf("%d",n);
		return (1);
	}
	else
	{
		binario(n/2); 
		printf("%d", n%2); 
		return 1;//só para sair da função, "nao serve pra nada"
	}
}
/* Explicação de recursão...imprime 5432112345
int f(n) { 
	if (n == 1) {
		 printf("1\n");
		 printf("1\n"); 
		return 1; 
	} 
	else { 
		printf("%d\n", n); 
		f(n-1); 
		printf("%d\n", n); 
		return 1;
 		} 
}*/

