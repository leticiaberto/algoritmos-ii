/* ================================================================== *
   Universidade Federal de Sao Carlos - UFSCar, Sorocaba

   Disciplina: Algoritmos e Programação 2
   Prof. Tiago A. Almeida

   Lista 06 - Exercício 03 - Monta Texto

   Instrucoes
   ----------

   Este arquivo contem o codigo que auxiliara no desenvolvimento do
   exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
   Dados do aluno:

   RA: 587354
   Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// declaracoes das funcoes
/* <<< DECLARE OS PROTOTIPOS DAS FUNCOES AQUI >>> */
char *alocaEspaco(int tamanho_trecho);
char *montaTexto(char *texto,int tamanho,char *trecho,int tamanho_trecho);
/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main(){

   char *texto = NULL; // ponteiro para armazenar o texto completo
   char *trecho = NULL; // ponteiro para armazenar o trecho de entrada
   int tamanho_trecho = 0; // variavel para armazenar o tamanho do trecho de entrada
   int tamanho = 0; // variavel para armazenar o tamanho do texto

   // recebe o tamanho do trecho
   scanf("%d",&tamanho_trecho);
   while(tamanho_trecho != 0) {
      
      // libera espaco anterior e aloca novo espaco
      free(trecho);
      trecho = alocaEspaco(tamanho_trecho);
      if(texto == NULL)
         texto = alocaEspaco(tamanho_trecho);

      // le trecho
      scanf("%*[ ]%[^\n]",trecho);

      // monta texto com novas informacoes recebidas
      tamanho = tamanho + tamanho_trecho;
      texto = montaTexto(texto,tamanho,trecho,tamanho_trecho);

      scanf("%d",&tamanho_trecho);
   }

   printf("%s\n",texto);

   // libera memória alocada
   free(texto);
   free(trecho);   
  
   return 0;
}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */
char *alocaEspaco(int tamanho_trecho)
{
	if (tamanho_trecho > 0)//só aloca se o tamanho for positivo
		return ((char *) malloc((tamanho_trecho + 1) * sizeof(char)));//+ 1 para o \0
	else
		return NULL;
}
char *montaTexto(char *texto,int tamanho,char *trecho,int tamanho_trecho)
{			
	if (tamanho_trecho < 0)
		texto[tamanho] = '\0';
		
	texto = (char *) realloc(texto, (tamanho + 1) * sizeof(char));
	
	if (tamanho_trecho > 0)//insere o texto
		strcat(texto,trecho);
	
	return texto;
}
