/* ================================================================== *
   Universidade Federal de Sao Carlos - UFSCar, Sorocaba

   Disciplina: Algoritmos e Programação 2
   Prof. Tiago A. Almeida

   Lista 06 - Exercício 02 - Xadrez

   Instrucoes
   ----------

   Este arquivo contem o codigo que auxiliara no desenvolvimento do
   exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
   Dados do aluno:

   RA: 587354
   Nome: Leticia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// times
#define TIME_PRETO 	100
#define TIME_BRANCO 	101

// tipos de pecas
#define PEAO	1
#define	BISPO	2
#define	TORRE	3
#define CAVALO	4
#define RAINHA	5
#define REI	6

/* <<< DECLARE O REGISTRO DA PECA AQUI >>> */
typedef struct peca
{
	//int linha;
	//int coluna;
	int tipo;
	int time;
}Peca;
// declaracoes das funcoes
/* <<< DECLARE OS PROTOTIPOS DAS FUNCOES AQUI >>> */
Peca** criaTabuleiro(int l,int c);
Peca criaPeca(int time, int tipo);
void posicionaPecaNoTabuleiro(Peca **tabuleiro, Peca peca, int pos_l, int pos_c);
int verificatime (int time,int l_orig,int c_orig, int c_dest, int l_dest, Peca **tabuleiro, Peca vazia);
int verificatimedestino(int time,Peca **tabuleiro,int l_dest,int c_dest,int l_orig,int c_orig);
void chegounaPosicao(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia);
void peao(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia);
void bispo(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia, int c);
void torre(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia);
void cavalo(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia);
void rei(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia);
void rainha(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia, int c, int l);
void executaJogada(Peca **tabuleiro, int l, int c, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas);
void liberaMemoria(Peca **tabuleiro,int l);
/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main(){

   // tabuleiro para armazenar as pecas em suas posicoes
   Peca **tabuleiro; 

   // variaveis para armazenar tamanho de linhas e colunas do tabuleiro
   int l,c; 
   l = c = 0;

   // variaveis para armazenar os resultados do jogo
   int jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas;
   jogadasValidas = pecasPretasDestruidas = pecasBrancasDestruidas = 0;

   // variaveis para fazer a jogada (linha e coluna de origem, linha e coluna de destino)
   int l_orig, c_orig, l_dest, c_dest;
   l_orig = c_orig = l_dest = c_dest = 0;
  
   // faz alocacao de memoria para o tabuleiro do jogo
   scanf("%d %d", &l, &c);
   tabuleiro = criaTabuleiro(l,c);

   int time, tipo,pos_l,pos_c; // variaveis para criar as pecas

   // le as pecas ateh encontrar o valor zero em time
   scanf("%d", &time);
   while(time != 0) {

      scanf("%d %d %d", &tipo, &pos_l, &pos_c);

      posicionaPecaNoTabuleiro(tabuleiro,criaPeca(time, tipo), pos_l, pos_c);

      scanf("%d", &time);

   }

   // le as jogadas ateh encontrar o valor zero em time
   scanf("%d", &time);
   while(time != 0) {

      scanf("%d %d %d %d", &l_orig, &c_orig, &l_dest, &c_dest);

      executaJogada(tabuleiro, l, c, time, l_orig, c_orig, l_dest, c_dest, &jogadasValidas, &pecasPretasDestruidas, &pecasBrancasDestruidas);
	  
      scanf("%d", &time);

   }

   printf("Total de jogadas validas: %d\n",jogadasValidas);
   printf("Total de pecas pretas destruidas: %d\n",pecasPretasDestruidas);
   printf("Total de pecas brancas destruidas: %d\n",pecasBrancasDestruidas);
  
   // libera memoria do tabuleiro e das pecas
   liberaMemoria(tabuleiro,l);

   return (0);
}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */
Peca** criaTabuleiro(int l,int c)
{
	//Peca **tabuleiro;
	int i,j;
	Peca vazia = {0,0};
	
	Peca **tabuleiro = (Peca **) malloc (l*sizeof(Peca*));
	//é uma matriz
	for (i=0; i<l; i++)
		tabuleiro[i] = (Peca *) malloc (c * sizeof(Peca));
		
	
	
	//(*tabuleiro)->tipo
	//(**tabuleiro).tipo
	
	//teste de alocação
	for (i = 0; i < l; i++) {
		for (j = 0; j < c; j++) {
			tabuleiro[i][j] = vazia;//zerar, pq senao vai pegar lixo
			//scanf("%d", &tabuleiro[i][j].tipo);
			
			//scanf("%d", &(*(tabuleiro + (i*c + j)))->tipo);
			
		}
	}
	return tabuleiro;	
}
Peca criaPeca(int time, int tipo)
{
	Peca peca;
	
	peca.tipo = tipo;
	peca.time = time;
	//printf("tipo: %d\n",peca.tipo);
	//printf("time: %d\n",peca.time);
	return peca;
}
void posicionaPecaNoTabuleiro(Peca **tabuleiro, Peca nova_peca, int pos_l, int pos_c)
{
//printf("time: %d\n",nova_peca.time);
	// tabuleiro[pos_l][pos_c].tipo = nova_peca.tipo;
	// tabuleiro[pos_l][pos_c].time = nova_peca.time;
	// equivalente
	tabuleiro[pos_l][pos_c] = nova_peca;
}
int verificatime (int time,int l_orig,int c_orig, int c_dest, int l_dest, Peca **tabuleiro, Peca vazia)//verifica se a peça da jogada é do time do jogador
{
	int flag;
	if (l_dest != l_orig || c_orig != c_dest)//testa se tentou colocar na mesma posição
	{
		if (time == tabuleiro[l_orig][c_orig].time && time != vazia.time)
			flag = 1;
		else
			flag = 0;
			//(*jogadasValidas)--;//a peça não é do time do jogador em questão
	}
	return flag;
}
int verificatimedestino(int time,Peca **tabuleiro,int l_dest,int c_dest,int l_orig,int c_orig)//verifica se a peça de destino é do mesmo time
{
	int flag;
	if (tabuleiro[l_orig][c_orig].time != tabuleiro[l_dest][c_dest].time)
		flag = 1;
	else
		flag = 0;
		//(*jogadasValidas)--;//a peça de destino é do mesmo time do jogador em questão
	return flag;
}
void chegounaPosicao(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia)
{
	(*jogadasValidas)++;
	if (tabuleiro[l_dest][c_dest].time == vazia.time)////nao tem peça no caminho, entao posiciona no local de destino, ja que a posição é vazia
	{
		tabuleiro[l_dest][c_dest] = tabuleiro[l_orig][c_orig];//recebe a peça
		tabuleiro[l_orig][c_orig] = vazia;//apaga a peça da posiçao inicial
	}
	else//tem peça do adversario no local, logo a destroi e posiciona no local
	{
		if (tabuleiro[l_dest][c_dest].time == TIME_PRETO)
			(*pecasPretasDestruidas)++;
		else
			(*pecasBrancasDestruidas)++;
						
		tabuleiro[l_dest][c_dest] = tabuleiro[l_orig][c_orig];//recebe a peça
		tabuleiro[l_orig][c_orig] = vazia;
	}
}
void peao(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia)
{
	if ((l_dest == l_orig + 1 || l_dest == l_orig - 1) && (c_dest == c_orig + 1 || c_dest == c_orig - 1))//destroi na diagonal
	{	
		(*jogadasValidas)++;
		if (tabuleiro[l_dest][c_dest].time == TIME_PRETO)
			(*pecasPretasDestruidas)++;
		else
			(*pecasBrancasDestruidas)++;
			tabuleiro[l_dest][c_dest] = tabuleiro[l_orig][c_orig];//recebe a peça
			tabuleiro[l_orig][c_orig] = vazia;
	}
	else
	{
		//if (c_dest != c_orig)//nao andou apenas em coluna
		//(*jogadasValidas)--;
		if (c_dest == c_orig)
		{
			if (l_dest == l_orig + 1 || l_dest == l_orig - 1)//apenas uma casa por jogada
				chegounaPosicao(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
		}	
	}
}
void bispo(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia, int c)
{
	int num_pecas = 0, i, j;
	if (c_dest != c_orig && l_dest != l_orig)//anda na diagonal
	{	
		if ((l_dest == l_orig + 1 || l_dest == l_orig - 1) && (c_dest == c_orig + 1 || c_dest == c_orig - 1)) 
			num_pecas = 0;
		else
		{
			if (l_dest < l_orig)//esta para cima
			{
				for (i=l_orig-1; i>l_dest; i--)
				{	if (c_dest < c_orig)
					{
						for (j=c_orig-1; j>c_dest; j--)
						{
							if (tabuleiro[i][j].time != vazia.time)//tem peça ate a casa anterior do destino. Vazia representa que nao tem peça
								num_pecas++;
						}
					}
					if (c_dest > c_orig)
					{
						for (j=c_orig+1; j<c_dest; j++)
						{
							if (tabuleiro[i][j].time != vazia.time)//tem peça ate a casa anterior do destino. Vazia representa que nao tem peça
								num_pecas++;								
						}
					}
				}
			}
			if (l_dest > l_orig)//esta para baixo
			{
				for (i=l_orig+1; i<l_dest; i++)
				{	if (c_dest < c_orig)
					{
						for (j=c_orig-1; j>c_dest; j--)
						{
							if (tabuleiro[i][j].time != vazia.time)//tem peça ate a casa anterior do destino. Vazia representa que nao tem peça
								num_pecas++;			
						}
					}
					if (c_dest > c_orig)
					{
						for (j=c_orig+1; j<c_dest; j++)
						{
							if (tabuleiro[i][j].time != vazia.time)//tem peça ate a casa anterior do destino. Vazia representa que nao tem peça
								num_pecas++;
						}
					}
				}
			}
		}
		if (num_pecas == 0)
			chegounaPosicao(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
	}
}

void torre(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia)
{
	int i, j, num_pecas = 0;
	if (l_dest == l_orig)//anda na horizontal
	{
		if (c_dest == c_orig - 1 || c_dest == c_orig + 1)
				num_pecas = 0;
		else
		{
			if (c_dest < c_orig)//esta antes da peça
			{
				for (j=c_orig-1; j> c_dest; j--)//vai ate a posição anterior do desejado
				{	
					if (tabuleiro[l_orig][j].time != vazia.time)//tem peça ate a casa anterior do destino. Vazia representa que nao tem peça
						num_pecas++;
				}
			
			}
			else
				if (c_dest > c_orig)//esta depois da peça
				{
					for (j=c_orig+1; j< c_dest; j++)//vai ate a posição anterior do desejado
					{	
						if (tabuleiro[l_orig][j].time != vazia.time)//tem peça ate a casa anterior do destino. Vazia representa que nao tem peça					
							num_pecas++;
					}
				}
		}
	}
	else
		if (c_dest == c_orig)//anda na vertical
		{
			if (l_dest == l_orig - 1 || l_dest == l_orig + 1)
				num_pecas = 0;
			else
			{
				if (l_dest < l_orig)//esta antes
				{
					for (i=l_orig-1; i>l_dest; i--)
					{
						if (tabuleiro[i][c_orig].time != vazia.time)//tem peça ate a casa anterior do destino. Vazia representa que nao tem peça
							num_pecas++;
					}
				}
				else
					if (l_dest > l_orig)
					{
						for (i=l_orig+1; i<l_dest; i++)
						{
							if (tabuleiro[i][c_orig].time != vazia.time)//tem peça ate a casa anterior do destino. Vazia representa que nao tem peça
								num_pecas++;
						}
					}
			}
		}
		
	if (num_pecas == 0)
		{
			chegounaPosicao(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
		}
}
void cavalo(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia)	
{
	if (c_dest == c_orig + 2 || c_dest == c_orig - 2 )//andou 2 na horizontal e 1 na vertical
	{
		if (l_dest == l_orig - 1 || l_dest == l_orig + 1)
		{	
			chegounaPosicao(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
				
		}
	}
	else
		if (l_dest == l_orig+ 2 || l_dest == l_orig- 2)//andou 2 na vertical e 1 na horizontal
		{
			if (c_dest == c_orig - 1 || c_dest == c_orig + 1)
			{	
				chegounaPosicao(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
			}
				
		}
}
void rei(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia)
{
	if ((c_dest == c_orig + 1 || c_dest == c_orig - 1)&& l_dest == l_orig)//andou na horizontal. Apenas uma casa por jogada
		chegounaPosicao(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
	else
		if ((l_dest == l_orig + 1 || l_dest == l_orig - 1) && c_dest == c_orig)//andou na vertical. Apenas uma casa por jogada
			chegounaPosicao(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
		else
			if (c_dest != c_orig && l_dest != l_orig)//anda na diagonal
				if ((l_dest == l_orig + 1 || l_dest == l_orig - 1) && (c_dest == c_orig + 1 || c_dest == c_orig - 1)) 
					chegounaPosicao(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
}
void rainha(Peca **tabuleiro, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas, Peca vazia, int c, int l)
{
	int dist_l = l_dest - l_orig;//verifica se nao andou fora da diagonal
	dist_l = dist_l < 0 ? dist_l * -1 : dist_l;
	
	int dist_c = c_dest - c_orig;//verifica se nao andou fora da diagonal
	dist_c = dist_c < 0 ? dist_c * -1 : dist_c;
	
	int flag = 0;
	//testa se o movimento nao é do cavalo
	if (c_dest == c_orig + 2 || c_dest == c_orig - 2 )//andou 2 na horizontal e 1 na vertical
		if (l_dest == l_orig - 1 || l_dest == l_orig + 1)
			flag = 1;
	else
		if (l_dest == l_orig+ 2 || l_dest == l_orig- 2)//andou 2 na vertical e 1 na horizontal
			if (c_dest == c_orig - 1 || c_dest == c_orig + 1)
				flag =1;
			
	if (flag==0)
	{		
		if (c_dest != c_orig && l_dest != l_orig && dist_l == dist_c )//anda na diagonal - movimento do Bispo	
			bispo(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia, c);
		else
			if (l_dest == l_orig || c_dest == c_orig)//anda na vertical ou horizontal - movimento da Torre
				torre(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
	}
}
void executaJogada(Peca **tabuleiro, int l, int c, int time, int l_orig, int c_orig, int l_dest, int c_dest,int *jogadasValidas, int *pecasPretasDestruidas, int *pecasBrancasDestruidas)
{
	int i,j,num_pecas = 0;
	Peca vazia = {0,0};
	//Movimentos do PEÃO (apenas uma casa por jogada, sempre na mesma coluna, pode andar para trás,pode destruir peças que estejam a uma casa de distancia e na diagonal)
	
	//posição valida(existe no tabuleiro)
	//if (l_dest > l || c_dest > c || l_dest < 0 || c_dest < 0)
		//(*jogadasValidas)--;
	if (l_dest < l && c_dest < c && l_dest >= 0 && c_dest >= 0)
	{
		if (verificatime(time,l_orig,c_orig,c_dest,l_dest,tabuleiro,vazia))//se for 1 a jogada é valida, senao nao
		{
			if (verificatimedestino(time,tabuleiro,l_dest,c_dest,l_orig,c_orig))//se for 1 a jogada é valida
			{
				if (tabuleiro[l_orig][c_orig].tipo == 1)//peão
					peao(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
				else
					if (tabuleiro[l_orig][c_orig].tipo == 2)//bispo
						bispo(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia, c);
					else
						if (tabuleiro[l_orig][c_orig].tipo == 3)//torre
							torre(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
						else
							if (tabuleiro[l_orig][c_orig].tipo == 4)//cavalo
								cavalo(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
							else
								if (tabuleiro[l_orig][c_orig].tipo == 5)//rainha
									rainha(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia, c, l);
								else
									if (tabuleiro[l_orig][c_orig].tipo == 6)//rei
										rei(tabuleiro, time, l_orig, c_orig, l_dest, c_dest, jogadasValidas, pecasPretasDestruidas, pecasBrancasDestruidas, vazia);
			}
		}
	}
	
	//Movimentos do BISPO (diagonal por mais de uma casa, mas sem pular peça)
	//Movimentos da TORRE (horizontal ou vertical por mais de uma casa, mas sem pular peça)
	//Movimentos do CAVALO (em "L" e pode pular peça)
	//Movimentos da RAINHA (pode fazer os mesmos movimentos que o rei, o bispo e a torre)
	//Movimentos do REI (Apenas uma casa por jogada em qualquer direção, pode destruir qualquer peça que esteja dentro do raio de uma casa)
}
void liberaMemoria(Peca **tabuleiro,int l)
{
	int i;
	
	for (i=0; i<l; i++)
		free(tabuleiro[i]);
	free(tabuleiro);
}

