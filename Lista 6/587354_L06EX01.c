/* ================================================================== *
   Universidade Federal de Sao Carlos - UFSCar, Sorocaba

   Disciplina: Algoritmos e Programação 2
   Prof. Tiago A. Almeida

   Lista 06 - Exercício 01 - Indentador de Código

   Instrucoes
   ----------

   Este arquivo contem o codigo que auxiliara no desenvolvimento do
   exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
   Dados do aluno:

   RA: 587354
   Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// declaracoes das funcoes
/* <<< DECLARE OS PROTOTIPOS DAS FUNCOES AQUI >>> */
char *alocaEspaco(int tamanho);
void indentaCodigo(char *codigoFonte, int tamanho); 
/* ------------------------------ */
/* ROTINA PRINCIPAL - NAO ALTERAR */
/* ------------------------------ */
int main(){

   char *codigoFonte = NULL; // ponteiro para armazenar o codigo fonte de entrada
   int tamanho = 0; // variavel para armazenar o tamanho do codigo fonte de entrada

   // recebe o tamanho do codigo fonte a ser indentado
   scanf("%d",&tamanho);

   // deve alocar espaço para a variavel buffer
   codigoFonte = alocaEspaco(tamanho);

   // verifica se a alocação deu certo
   if (codigoFonte == NULL)
   {
      printf("Problema ao alocar memória.\n");
      exit(1);
   }

   // le o codigo a ser indentado
   scanf("%[^\n]",codigoFonte);

   // deve imprimir o codigo indentado
   indentaCodigo(codigoFonte, tamanho); 

   // libera a memória
   free(codigoFonte);
  
   return 0;
}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */
char *alocaEspaco(int tamanho)
{
	return ((char *) malloc ((tamanho + 1) * sizeof(char)));//+1 para o \0
}
//O programa deve funcionar para as estruturas if,else,while e for e ser capaz de indentar funco ̃es al ́em da main. 
void indentaCodigo(char *codigoFonte, int tamanho)
{
	int i,cont=0, j,aux=0,l=0,flag=0;
	
	for (i=0; i<=tamanho; i++)
	{	//printf("C: %c\n",codigoFonte[i]);
		if (codigoFonte[i] ==' ' && (codigoFonte[i-1] ==';' || codigoFonte[i-1] =='}' || codigoFonte[i-1] =='>' || codigoFonte[i-1] =='{' || codigoFonte[i+1] =='#') && aux==0)
			continue;
			
		if (codigoFonte[i] == '}'&& aux==0)//fim de uma função
		{	
			if (cont>1)
				for (j=0; j< cont-1; j++)
					printf("\t");
			printf("%c",codigoFonte[i]);
			printf ("\n");		
			cont--;
			//printf("\t");
		}
		else
		{
			if (cont != 0 && (codigoFonte[i-2] == '}' && codigoFonte[i-1] == ' ' || codigoFonte[i-1] == '}')&& aux==0 )//esta dentro de uma funçao e precisa de tab em cada linha
			{	//flag=0;
				for (j=0; j< cont; j++)
					printf("\t");
				printf("%c",codigoFonte[i]);
			}
			else
			printf("%c",codigoFonte[i]);
		}
		if (codigoFonte[i-1] == 'h' && codigoFonte[i] == '>')//includes
		{	printf ("\n");}
		if (codigoFonte[i] == '(')
		{	aux++;}
		if (codigoFonte[i] == ')')
		{	aux--;
			//printf ("\n");
		}
		if (codigoFonte[i] == ';' && aux==0)//final de linha
		{
			printf ("\n");
			
			if (cont != 0 && (codigoFonte[i+1] != '}' && codigoFonte[i+2] != ' ') && (codigoFonte[i+1] != ' ' || codigoFonte[i+2] != '}'))//esta dentro de uma funçao e precisa de tab em cada linha -- certa gambiarra,o original é cont != 0 && codigoFonte[i+1] != '}'
			{	for (j=0; j< cont; j++)
					printf("\t");
			}
		}
		if (codigoFonte[i] == '{' && aux==0)//começo de uma função e tab
		{
			printf ("\n");cont++;
			for (j=0; j< cont; j++)
				printf("\t");
			//cont++;
		}
		
		
	}//for
	
	//printf("%s",codigoFonte);
}
