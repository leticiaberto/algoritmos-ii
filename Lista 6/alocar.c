#include <stdio.h>
#include <stdlib.h>

#define POS 5
#define TRUE 1
#define FALSE 0

typedef int Bool;//retorna se a alocação deu certo ou não

Bool checarAlocacao (void *pointer)
{
	return ((pointer == NULL) ? FALSE : TRUE);
}

int main(void)
{
	int *p;
	int valor, i, tamanho = POS, qtElementos = 0;

	p = (int *)malloc (POS * sizeof(int));

	if (checarAlocacao (p) == FALSE)
	{
		printf ("Memória insuficiente!\n");
		return (EXIT_FAILURE); //deu pau!
	}

	do 
	{	
		if (tamanho == qtElementos - 1)
		{
			tamanho += POS;
			p = (int *)realloc (p, tamanho * sizeof(int));

			if (checarAlocacao (p) == FALSE)
			{
				printf ("Memória insuficiente!\n");
				return (EXIT_FAILURE); //deu pau!
			}
		}

		scanf("%d",&valor);

		if (valor != -1)
			p[qtElementos++] = valor;

	}while (valor != -1);
	
	for (i = 0; i < qtElementos; i++)
		printf ("%d ", p[i]);

	printf ("\n");

	free (p);

	return (EXIT_SUCCESS);//igual return (0)
}
