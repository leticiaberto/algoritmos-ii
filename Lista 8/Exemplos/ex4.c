//Imprimir o vetor do ex3 sem o tamanho
#include <stdio.h>

int main()
{
	FILE *fr;
	int num, i, valor;
	fr = fopen("ex3.txt", "r");
	
	if (fr == NULL){
		perror("Erro ao abrir o arquivo\n");
		return 1;
	}
	
	fscanf(fr,"%d", &num);//le o tamanho do vetor
	for (i=0; i<num; i++)
	{
		fscanf(fr, "%d", &valor);
		printf("%d\n",valor);
	}
	fclose(fr);
	return 0;
}
