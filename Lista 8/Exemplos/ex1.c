//Receber um valor do usuario e salvar em um arquivo
#include <stdio.h>

int main(void)
{
	FILE *fw;
	int num;
	fw = fopen("ex1.txt","w");
	
	if (fw == NULL)
	{	perror("ex1.txt");
		return 1;
	}
	
	scanf ("%d",&num);
	fprintf(fw, "%d\n", num);
	fclose (fw);
	return 0;
}
