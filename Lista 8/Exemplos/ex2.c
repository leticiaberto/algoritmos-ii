//imprimir o conteudo do arquivo gerado no ex1
#include <stdio.h>

int main()
{
	FILE *fw;
	int num;
	fw = fopen("ex1.txt", "r");
	
	if (fw == NULL){
		perror("Erro ao abrir o arquivo\n");
		return 1;
	}
	
	fscanf(fw,"%d", &num);
	printf("%d\n",num);
	fclose(fw);
	return 0;
}
