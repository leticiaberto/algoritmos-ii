//Receber valores e salvar um vetor em um arquivo
#include <stdio.h>

int main()
{
	FILE *fr;
	int tam,i,v; 
	
	fr = fopen("ex3.txt", "w");
	if (fr == NULL){
		perror("Erro ao abrir o arquivo\n");
		return 1;
	}
	//le o tamanho do vetor
	scanf("%d",&tam);
	
	fprintf(fr, "%d\n", tam);
	
	for (i = 0; i < tam; i++)
	{
		scanf("%d",&v);
		fprintf(fr,"%d\n",v);
	}
	
	fclose(fr);
	
	return 0;
}
