/* Nome: Letícia Mara Berto
RA: 587354 */
#include <stdio.h>
#include<stdlib.h>
typedef struct _valores
{
	char c;
	int inteiro;
	float real;
}Valores;
void ordenar(Valores *vet, int l, FILE *fr, char nome[]);
void ler(FILE *fr, char nome[]);
void imprimir(Valores *vet, int l, FILE *fr, char nome[]);
int main()
{
	FILE *fr;
	char nome[100], c;
	
	scanf("%[^\n]",nome);
	
	fr = fopen(nome, "r");//só para ler
	if (fr == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	ler(fr,nome);
	
	fr = fopen(nome, "r");
	/*while(fscanf(fr, "%c", &c) != EOF)
		printf("%c",c);*/
	fclose(fr);
	return 0;
}
void ler(FILE *fr, char nome[])
{
	int l,i;
	
	fscanf(fr, "%d\n", &l); //lê o numero de linhas
	
	Valores *vet = (Valores *) malloc (l * sizeof(Valores));
	
	for (i = 0; i< l; i++)
	{
		fscanf(fr, "%c %d %f\n",&vet[i].c, &vet[i].inteiro, &vet[i].real);//\n pra desconsiderar na leitura do arquivo
		
	}	
	
	ordenar(vet,l,fr,nome);
}
void ordenar(Valores *vet, int l, FILE *fr, char nome[])
{
	Valores aux;
	int j, i;
       
      for(i = 0; i < l-1; i++)
      {
        for(j=i+1; j < l ; j++)
	 	{
            if(vet[i].c > vet[j].c)
	   		{
               aux = vet[i];
               vet[i] = vet[j];
               vet[j] = aux;
            }
         }
      }  
     fclose(fr);
     imprimir(vet,l,fr,nome);
}
void imprimir(Valores *vet, int l, FILE *fr, char nome[])
{
	int i;
	char num;
	fr = fopen(nome, "w");//salvar as alterações
	fprintf(fr, "%d\n",l);
	for (i = 0;i < l; i++)
	{	fprintf(fr, "%c %d %g",vet[i].c, vet[i].inteiro, vet[i].real);
		fprintf(fr,"\n");
	}
	fclose(fr);
	free(vet);
}
