/* Nome: Letícia Mara Berto
RA: 587354 */
#include <stdio.h>
int gerarArquivo(FILE *fr,int n,int m,char nome[]);
int lerImprimirArquivo(FILE *fr,int n,int m,char nome[]);

int main()
{
	FILE *fr;
	int n,m;
	char nome[100];
	
	scanf("%[^\n]",nome);
	scanf("%d %d", &n, &m);
		
	gerarArquivo(fr,n,m,nome);
	lerImprimirArquivo(fr,n,m,nome);
	
	return 0;
}
int gerarArquivo(FILE *fr,int n,int m,char nome[])
{
	int i,j;
	fr = fopen(nome, "w");
	if (fr == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	fprintf(fr, "%d %d\n", n,m);
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
			fprintf(fr,"%d ", (i*m) + j);
		fprintf(fr,"\n");
	}
	fclose(fr);
}
int lerImprimirArquivo(FILE *fr,int n,int m,char nome[])
{
	char c;
	fr = fopen(nome, "r");
	if (fr == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	while(fscanf(fr, "%c", &c) != EOF)
		printf("%c",c);
	fclose(fr);
}
