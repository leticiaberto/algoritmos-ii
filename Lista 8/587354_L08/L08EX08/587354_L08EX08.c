/*Letícia Mara Berto
RA: 587354 */
#include <stdio.h>

#define MAX 100

void grava(int const vet1[], FILE *fo, int c1);

int main()
{
	FILE *fi, *fo;
	char arqin[100], arqout[100];
	int cont1, cont2, cont3, cont4,i,//contadores do tamanho de cada vetor
		 t=0,//indice dos vetores
		 flag = 0,//leitura da primeira linha armazena os valores como contadores dos vetores
		 va1, va2, va3, va4,//valores a cada leitura
		 c1, c2, c3, c4,//contadores auxiliares para dps da leitura, pois decrementaremos o contador original de cada vetor
		 col; //coluna desejada
	int vet1[MAX],vet2[MAX],vet3[MAX],vet4[MAX];
	
	scanf("%[^\n]%*c",arqin);
	scanf("%d",&col);
	scanf("\n%[^\n]",arqout);

	fi = fopen(arqin, "r");
	fo = fopen(arqout, "w");

	if(fi == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}

	if(fo == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	fscanf(fi, "%d %d %d %d", &cont1, &cont2, &cont3, &cont4);
	
	c1 = cont1;
	c2 = cont2;
	c3 = cont3;
	c4 = cont4;
	//printf("cont = %d",cont1);
			
		while(cont1 > 0 || cont2 > 0 || cont3 > 0 || cont4 > 0)
		{
		//printf("entrou");	
		if(cont1 > 0)
			{
				fscanf(fi, "%d",&vet1[t]);
				cont1--;
			}
			if(cont2 > 0)
			{
				fscanf(fi, "%d",&vet2[t]);
				cont2--;
			}
			if(cont3 > 0)
			{
				fscanf(fi, "%d",&vet3[t]);
				cont3--;
			}
			if(cont4 > 0)
			{
				fscanf(fi, "%d",&vet4[t]);
				cont4--;
			}
			t++;
		
		
	}
	
	switch (col)
	{
		case 0:
			for (i = 0; i < c1; i++)
				printf("%d\n",vet1[i]);
			grava(vet1, fo, c1);
		break;
		case 1:
			for (i = 0; i < c2; i++)
				printf("%d\n",vet2[i]);
			grava(vet2, fo, c2);
		break;
		case 2:
			for (i = 0; i < c3; i++)
				printf("%d\n",vet3[i]);
			grava(vet3, fo, c3);
		break;
		case 3:
			for (i = 0; i < c4; i++)
				printf("%d\n",vet4[i]);
			grava(vet4, fo, c4);
		break;
	}	
	fclose(fi);
	fclose(fo);
	return 0;
}
void grava(int const vet1[], FILE *fo, int c1)
{
	int i;
	for (i = 0; i < c1; i++)
		fprintf(fo, "%d\n", vet1[i]);
}
