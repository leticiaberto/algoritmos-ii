/*Letícia Mara Berto
RA: 587354 */

//Caso a pagina nao tenha 60 linhas, completa com \n ate atingir o limite e depois coloca a numeração da pagina
#include <stdio.h>

int main()
{
	FILE *fi, *fo;
	char entrada[1000], saida[1000], c;
	int carac = 0, pag = 1, linha = 0,i;
	scanf("%[^\n]%*c",entrada);
	scanf("%[^\n]",saida);
	
	fi = fopen(entrada, "r");
	if (fi == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	fo = fopen(saida, "w");
	if (fo == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	while(fscanf(fi, "%c", &c) != EOF)
	{
		if (carac < 60)
		{	
			fprintf(fo, "%c",c);
			carac++;
		}
		if (carac == 60)
		{	
			carac = 0;
			fprintf(fo,"\n");
			linha++;
		}
		if (linha == 60)
		{
			linha = 0;
			fprintf(fo,"\n");
			for(i = 0; i < 59; i++)
				fprintf(fo, " ");
			fprintf(fo, "%d\n",pag);
			for(i = 0; i < 60; i++)
				fprintf(fo, "-");
			fprintf(fo,"\n");//para começar a nova pagina
			pag++;
		}
	}
	if (linha != 0)
	{
		while(linha < 61)//porque sao 60 linhas
		{
	
			fprintf(fo,"\n");
			linha++;
		}
		for(i = 0; i < 59; i++)
					fprintf(fo, " ");
				fprintf(fo, "%d\n",pag);
				for(i = 0; i < 60; i++)
					fprintf(fo, "-");
	}
	fclose(fi);
	fclose(fo);
	return 0;
}
