/* Nome: Letícia Mara Berto
RA: 587354 */
#include <stdio.h>

int main()
{
	FILE *fr;
	char num;
	char nome[100];
	
	scanf("%[^\n]", nome);
	
	fr = fopen(nome, "r");
	
	if (fr == NULL)
	{
		perror("Erro ao abrir o arquivo\n");
		return 1;
	}
	
	while (fscanf(fr, "%c", &num) != EOF)
		printf("%c", num);

	fclose(fr);	
  
	return 0;
}
