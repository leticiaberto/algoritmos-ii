/* Nome: Letícia Mara Berto
RA: 587354 */
#include <stdio.h>
int main()
{
	FILE *f1, *f2;
	char nome1[100], nome2[100],c;
	
	scanf("%[^\n]%*c",nome1);
	f1 = fopen(nome1, "r");
	if (f1 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	scanf("%[^\n]",nome2);
	f2 = fopen(nome2, "w");
	if (f2 == NULL)
	{
		perror("Erro ao abrir o arquivo");
		return 1;
	}
	
	while(fscanf(f1, "%c", &c) != EOF)
	{
		if (isalpha(c))
			fprintf(f2,"%c",c);
	}
	
	fclose(f1);
	fclose(f2);
	return 0;
}
